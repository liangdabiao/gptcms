
SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for `kt_base_agent`
-- ----------------------------
DROP TABLE IF EXISTS `kt_base_agent`;
CREATE TABLE `kt_base_agent` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `un` varchar(20) NOT NULL COMMENT '用户名',
  `pwd` varchar(200) DEFAULT NULL COMMENT '密码',
  `key_word` varchar(100) DEFAULT NULL COMMENT '网站关键词',
  `describe` varchar(255) DEFAULT NULL COMMENT '网站描述',
  `company_name` varchar(100) DEFAULT NULL COMMENT '公司名称',
  `company_address` varchar(255) DEFAULT NULL COMMENT '公司地址',
  `record_number` varchar(100) DEFAULT NULL COMMENT '备案号',
  `gzh_code` varchar(255) DEFAULT NULL COMMENT '公众号二维码',
  `kf_code` varchar(255) DEFAULT '' COMMENT '客服二维码',
  `email` varchar(50) DEFAULT NULL COMMENT '邮箱',
  `telephone` varchar(20) DEFAULT NULL COMMENT '联系电话',
  `isadmin` tinyint(1) DEFAULT '0' COMMENT '是否是管理员',
  `balance` decimal(10,2) DEFAULT NULL COMMENT '余额',
  `domain` varchar(255) DEFAULT NULL COMMENT '网站域名',
  `webname` varchar(255) DEFAULT '' COMMENT '网站名称',
  `webtitle` varchar(255) DEFAULT NULL COMMENT '网站标题',
  `qq` varchar(1024) DEFAULT NULL COMMENT '联系QQ',
  `pid` int(11) unsigned DEFAULT NULL COMMENT '开通人员id',
  `rtime` datetime DEFAULT NULL COMMENT '注册时间',
  `isstop` tinyint(1) DEFAULT '0' COMMENT '是否禁用 1:开启  0:禁用',
  `level` int(11) DEFAULT '1' COMMENT '代理级别',
  `register_check` tinyint(1) DEFAULT '1' COMMENT '验证码功能（1开启，2关闭）',
  `agency_token` varchar(255) DEFAULT NULL COMMENT '登录时获取的token',
  `expire_time` varchar(255) DEFAULT NULL COMMENT '登录token到期时间',
  `copyright` varchar(255) DEFAULT NULL COMMENT '版权',
  `registration_audit` int(10) DEFAULT NULL COMMENT '注册审核 （1开启，2关闭）',
  `user_logo` varchar(255) DEFAULT NULL COMMENT '用户后台LOGO',
  `login_logo` varchar(255) DEFAULT NULL COMMENT '登录页LOGO',
  `login_background_status` varchar(255) DEFAULT NULL COMMENT '1: 默认 2:自定义',
  `login_background` varchar(255) DEFAULT NULL COMMENT '登录页背景图',
  `status` int(10) DEFAULT '1' COMMENT '0为停用1为启用',
  `remark` varchar(255) DEFAULT NULL COMMENT '备注',
  `lasttime` datetime DEFAULT NULL COMMENT '最后登录时间',
  `pc_official` tinyint(1) DEFAULT NULL COMMENT 'PC官网1开启 2关闭',
  `register_setmeal` int(11) DEFAULT NULL,
  `register_setmeal_specsid` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=20 DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC COMMENT='代理用户表';

-- ----------------------------
-- Records of kt_base_agent
-- ----------------------------
INSERT INTO `kt_base_agent` VALUES ('1', 'admin', 'e10adc3949ba59abbe56e057f20f883e', '狂团', '狂团', '狂团', '狂团', '粤ICP备18075561号', 'http://demo.kt8.cn/storage/upload/base/2022-09-07/6318bc386de87.png', 'http://demo.kt8.cn/storage/upload/base/2022-09-07/6318bc31cdf5b.png', null, '4001188032', '1', '64953003.00', 'demo.kt8.cn', 'KtAdmin', '狂团框架管理系统', '', '0', '2022-08-13 14:10:33', '1', null, '2', '984a3c1a-f2fd-11ed-815a-00163e003301', '1684746580', '狂团KtAdmin框架', '1', 'http://weidogstest.oss-cn-beijing.aliyuncs.com/base/base_63172fd9d4150.png', 'http://weidogstest.oss-cn-beijing.aliyuncs.com/base/base_63172fdf62914.png', '1', 'http://weidogstest.oss-cn-beijing.aliyuncs.com/949c6004e2ceb22f5fde1f650befa0fa.png', '1', null, '2023-05-15 16:50:51', '2', null, null);

-- ----------------------------
-- Table structure for `kt_base_agent_apporder`
-- ----------------------------
DROP TABLE IF EXISTS `kt_base_agent_apporder`;
CREATE TABLE `kt_base_agent_apporder` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `uid` int(11) DEFAULT NULL,
  `wid` int(11) DEFAULT NULL,
  `content` varchar(255) DEFAULT NULL COMMENT '内容',
  `app_id` int(11) DEFAULT NULL COMMENT '应用id',
  `price` decimal(10,2) DEFAULT NULL COMMENT '价格',
  `create_time` datetime DEFAULT NULL,
  `openapp_id` int(11) DEFAULT NULL COMMENT '购买应用id',
  `specs_id` int(1) DEFAULT NULL COMMENT '规格id',
  `bh` varchar(255) DEFAULT NULL COMMENT '订单编号',
  `specs_content` text COMMENT '规格内容',
  `discount_price` decimal(10,2) DEFAULT NULL COMMENT '折后价',
  `setmeal_type` tinyint(1) NOT NULL DEFAULT '1' COMMENT '1: 单个开通  2:套餐开通',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;

-- ----------------------------
-- Records of kt_base_agent_apporder
-- ----------------------------
INSERT INTO `kt_base_agent_apporder` VALUES ('1', '1', '1', null, '13', '1000.00', '2023-05-15 16:56:42', null, '1', '202305151656424020|1', '{\"id\":1,\"duration\":\"1\",\"price\":\"1000\",\"old_price\":\"1000\",\"duration_type\":\"3\",\"index\":0}', '1000.00', '1');

-- ----------------------------
-- Table structure for `kt_base_agent_recharge_record`
-- ----------------------------
DROP TABLE IF EXISTS `kt_base_agent_recharge_record`;
CREATE TABLE `kt_base_agent_recharge_record` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL COMMENT 'base_agent',
  `money` double NOT NULL COMMENT '充值金额',
  `recharge_time` datetime NOT NULL COMMENT '充值时间',
  `out_trade_no` varchar(255) NOT NULL COMMENT '订单号',
  `type` int(10) DEFAULT '1' COMMENT '1为充值，2为扣除，3为支付',
  `status` int(11) DEFAULT '1',
  `agid` int(10) DEFAULT NULL COMMENT '充值方或者扣除方id',
  `remark` varchar(255) DEFAULT NULL COMMENT '备注',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=33 DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC COMMENT='代理充值记录';

-- ----------------------------
-- Records of kt_base_agent_recharge_record
-- ----------------------------

-- ----------------------------
-- Table structure for `kt_base_aliai_config`
-- ----------------------------
DROP TABLE IF EXISTS `kt_base_aliai_config`;
CREATE TABLE `kt_base_aliai_config` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `uid` int(11) DEFAULT NULL,
  `accesskey_id` varchar(255) DEFAULT NULL,
  `accesskey_secret` varchar(255) DEFAULT NULL,
  `region` varchar(255) DEFAULT NULL,
  `appkey` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;

-- ----------------------------
-- Records of kt_base_aliai_config
-- ----------------------------

-- ----------------------------
-- Table structure for `kt_base_app_package`
-- ----------------------------
DROP TABLE IF EXISTS `kt_base_app_package`;
CREATE TABLE `kt_base_app_package` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `uid` int(11) DEFAULT NULL COMMENT '代理ID',
  `name` varchar(255) DEFAULT NULL COMMENT '套餐名称',
  `specs` longtext COMMENT '规格{duration:时长；duration_type:1:天2:月3:年；old_price:原价;price:售价}',
  `apps` longtext COMMENT '包含应用',
  `sort` int(11) DEFAULT NULL COMMENT '排序',
  `ctime` datetime DEFAULT NULL,
  `utime` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;

-- ----------------------------
-- Records of kt_base_app_package
-- ----------------------------

-- ----------------------------
-- Table structure for `kt_base_app_store`
-- ----------------------------
DROP TABLE IF EXISTS `kt_base_app_store`;
CREATE TABLE `kt_base_app_store` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `pro_id` int(11) DEFAULT NULL COMMENT '商品id',
  `pro_name` varchar(255) DEFAULT NULL COMMENT '商品名称',
  `app_logo` varchar(255) DEFAULT NULL COMMENT '应用logo',
  `app_name` varchar(255) DEFAULT NULL COMMENT '应用名称',
  `price` decimal(10,2) DEFAULT NULL COMMENT '实际售价',
  `install_num` int(11) DEFAULT NULL COMMENT '安装量',
  `lastsj` datetime DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=28 DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;

-- ----------------------------
-- Records of kt_base_app_store
-- ----------------------------
INSERT INTO `kt_base_app_store` VALUES ('1', '2067', 'GPTCMS-人工智能助理，支持多端，支持VIP付费，UI精美', 'https://addon8.oss-cn-shenzhen.aliyuncs.com/image/1/168412107956978.jpg', 'GPTCMS', '1980.00', '238', '2023-05-15 16:50:42');
INSERT INTO `kt_base_app_store` VALUES ('2', '2034', 'GPT极速版-免费开源人工智能助理', 'https://addon8.oss-cn-shenzhen.aliyuncs.com/image/2239/168239271432571.png', 'GPT极速版', '0.00', '639', '2023-05-15 16:35:32');
INSERT INTO `kt_base_app_store` VALUES ('3', '1725', '企微SCRM系统-免费开源版下载专用链接-CRMUU-企微内部接口版', 'https://addon8.oss-cn-shenzhen.aliyuncs.com/image/1287/166694224213326.png', 'CRMUU-企微SCRM', '0.00', '999', '2023-05-15 16:25:38');
INSERT INTO `kt_base_app_store` VALUES ('4', '1684', '企业微信第三方免费源码底层-服务商代开发自建应用底层', 'https://addon8.oss-cn-shenzhen.aliyuncs.com/image/1287/166694224213326.png', '企微底层', '0.00', '335', '2023-05-15 16:25:01');
INSERT INTO `kt_base_app_store` VALUES ('5', '1683', '微信小程序在线提审发布系统源码微信开放平台对接系统', 'https://addon8.oss-cn-shenzhen.aliyuncs.com/image/2239/166978873763004.png', '微信小程序在线提审发布系统', '0.00', '679', '2023-05-15 16:00:09');
INSERT INTO `kt_base_app_store` VALUES ('6', '1778', '生意好打通插件-打通生意好系统统一化管理', 'https://addon8.oss-cn-shenzhen.aliyuncs.com/image/1/167308460433716.jpg', '生意好打通插件', '0.00', '56', '2023-05-15 15:59:50');
INSERT INTO `kt_base_app_store` VALUES ('7', '1779', '德创恋爱话术库打通插件-打通德创恋爱话术库统一化管理', 'https://addon8.oss-cn-shenzhen.aliyuncs.com/image/1/167308448714799.jpg', '德创恋爱打通插件', '0.00', '80', '2023-05-15 15:59:40');
INSERT INTO `kt_base_app_store` VALUES ('8', '1786', '乐活云红包括客系统打通插件-打通乐活云红包系统统一化管理', 'https://addon8.oss-cn-shenzhen.aliyuncs.com/image/1/167326164439939.jpg', '乐活云红包打通插件', '0.00', '85', '2023-05-15 15:59:32');
INSERT INTO `kt_base_app_store` VALUES ('9', '1755', '企微魔盒打通插件-打通企微魔盒统一化管理', 'https://addon8.oss-cn-shenzhen.aliyuncs.com/image/2239/166364337943434.png', '企微魔盒打通', '0.00', '277', '2023-05-15 15:59:12');
INSERT INTO `kt_base_app_store` VALUES ('10', '1798', 'VIPCARD打通插件-打通vipcard系统统一化管理', 'https://addon8.oss-cn-shenzhen.aliyuncs.com/image/1/167342994836482.jpg', 'vipcard打通插件', '0.00', '88', '2023-05-15 15:59:03');
INSERT INTO `kt_base_app_store` VALUES ('11', '1780', '云贝餐饮打通插件-打通云贝餐饮统一化管理', 'https://addon8.oss-cn-shenzhen.aliyuncs.com/image/1/167308616341110.jpg', '云贝餐饮打通插件', '0.00', '106', '2023-05-15 15:58:56');
INSERT INTO `kt_base_app_store` VALUES ('12', '1809', '全端云打通插件-打通全端云系统统一化管理', 'https://addon8.oss-cn-shenzhen.aliyuncs.com/image/1/167392729915606.jpg', '全端云打通插件', '0.00', '116', '2023-05-15 15:58:41');
INSERT INTO `kt_base_app_store` VALUES ('13', '1750', 'D音引流私信小卡片管理系统-无限账号-无需接口', 'https://addon8.oss-cn-shenzhen.aliyuncs.com/image/2239/166364337943434.png', '引流小卡片', '0.00', '577', '2023-05-15 15:58:38');
INSERT INTO `kt_base_app_store` VALUES ('14', '1751', '微信引流外链系统-微信外部跳转链接生成器-引流私域神器', 'https://addon8.oss-cn-shenzhen.aliyuncs.com/image/2239/166364337943434.png', '引流外链', '0.00', '516', '2023-05-15 15:58:31');
INSERT INTO `kt_base_app_store` VALUES ('15', '1756', '快速注册小程序系统-免300元认证费快速注册小程序', 'https://addon8.oss-cn-shenzhen.aliyuncs.com/image/2239/166978867419819.png', '快速注册小程序', '0.00', '746', '2023-05-15 15:58:27');
INSERT INTO `kt_base_app_store` VALUES ('16', '1159', 'WiFi赚钱宝WiFi源码WIFI拓客WIFI大师正版系统出售', 'https://addon8.oss-cn-shenzhen.aliyuncs.com/image/2199/165224112025844.jpg', 'WiFi赚钱宝', '2699.00', '173', '2023-05-15 09:58:10');
INSERT INTO `kt_base_app_store` VALUES ('17', '1749', '头像大师流量主赚钱正版系统', 'https://addon8.oss-cn-shenzhen.aliyuncs.com/image/574/167305722655429.png', '头像大师', '0.00', '324', '2023-05-13 23:37:47');
INSERT INTO `kt_base_app_store` VALUES ('18', '1783', '点大商城打通插件-打通点大商城系统统一化管理', 'https://addon8.oss-cn-shenzhen.aliyuncs.com/image/1/167325332643349.jpg', '点大商城对接插件', '0.00', '94', '2023-05-13 10:35:07');
INSERT INTO `kt_base_app_store` VALUES ('19', '1906', 'D音本地生活服务商系统-团购服务商系统-服务商员工分佣系统', 'https://addon8.oss-cn-shenzhen.aliyuncs.com/image/1/167835839648411.jpg', '抖裂变D音本地生活服务商系统', '29800.00', '5', '2023-05-09 17:43:43');
INSERT INTO `kt_base_app_store` VALUES ('20', '1644', '社群大师社群赚钱社群拓客正版系统', 'https://addon8.oss-cn-shenzhen.aliyuncs.com/image/574/166928087010455.png', '社群大师', '1980.00', '71', '2023-04-26 14:10:52');
INSERT INTO `kt_base_app_store` VALUES ('21', '1578', '抖裂变®-快抖矩阵系统-无限多开版', 'https://addon8.oss-cn-shenzhen.aliyuncs.com/image/1287/167167455237841.jpg', '企微魔盒-快抖爆客', '9800.00', '99', '2023-04-17 15:01:59');
INSERT INTO `kt_base_app_store` VALUES ('22', '1863', '企微魔盒Pro版-企微SCRM系统独立版版-无限账号', 'https://addon8.oss-cn-shenzhen.aliyuncs.com/image/1287/166694224213326.png', '企微魔盒pro版', '12800.00', '12', '2023-04-13 12:18:28');
INSERT INTO `kt_base_app_store` VALUES ('23', '1682', '仿企微魔盒PC官网模板源码', 'https://addon8.oss-cn-shenzhen.aliyuncs.com/image/53/163486945544051.png', '仿企微魔盒PC官网', '199.00', '5', '2023-03-18 12:28:14');
INSERT INTO `kt_base_app_store` VALUES ('24', '1864', '抖裂变®-D音企业号管理系统-无限账号版', 'https://addon8.oss-cn-shenzhen.aliyuncs.com/image/1287/166694224213326.png', '抖裂变企业号系统', '9800.00', '6', '2023-02-23 11:30:40');
INSERT INTO `kt_base_app_store` VALUES ('25', '1700', '仿快抖爆客PC官网模板源码', 'https://addon8.oss-cn-shenzhen.aliyuncs.com/image/1287/166694224213326.png', '仿快抖爆客官网模板', '199.00', '10', '2022-12-22 14:22:43');
INSERT INTO `kt_base_app_store` VALUES ('26', '1720', 'CRMUU-企微SCRM系统-企业内部接口版正版商业授权', 'https://addon8.oss-cn-shenzhen.aliyuncs.com/upload/1287/1671611655-53/8dPnA2s9aWW9ZTp-1673488452-1287.jpg', 'CRMUU-企微SCRM系统-企业内部接口版正版商业授权', '4999.00', '50', '2022-12-21 16:34:15');
INSERT INTO `kt_base_app_store` VALUES ('27', '1688', '微信客服源码系统-微信客服第三方源码系统', 'https://addon8.oss-cn-shenzhen.aliyuncs.com/upload/53/1669855305-53/xFE8CGFKNSCe7am-1673445112-53.jpg', '微信客服源码系统-微信客服第三方源码系统', '1999.00', '10', '2022-12-01 08:41:45');

-- ----------------------------
-- Table structure for `kt_base_baiduai_config`
-- ----------------------------
DROP TABLE IF EXISTS `kt_base_baiduai_config`;
CREATE TABLE `kt_base_baiduai_config` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `uid` int(11) DEFAULT NULL,
  `appid` int(11) DEFAULT NULL,
  `apikey` varchar(255) DEFAULT NULL,
  `secretkey` varchar(255) DEFAULT NULL,
  `token` varchar(255) DEFAULT NULL COMMENT 'token',
  `token_expire` int(11) DEFAULT NULL COMMENT 'token有效期 时间戳',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;

-- ----------------------------
-- Records of kt_base_baiduai_config
-- ----------------------------

-- ----------------------------
-- Table structure for `kt_base_chatglm_config`
-- ----------------------------
DROP TABLE IF EXISTS `kt_base_chatglm_config`;
CREATE TABLE `kt_base_chatglm_config` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `uid` int(11) DEFAULT NULL,
  `api_key` varchar(255) DEFAULT NULL,
  `public_key` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;

-- ----------------------------
-- Records of kt_base_chatglm_config
-- ----------------------------

-- ----------------------------
-- Table structure for `kt_base_content_security`
-- ----------------------------
DROP TABLE IF EXISTS `kt_base_content_security`;
CREATE TABLE `kt_base_content_security` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `uid` int(11) DEFAULT NULL,
  `question_system` tinyint(1) DEFAULT '1' COMMENT '提问审核 系统词库审核  1开启 0关闭',
  `question_deal` tinyint(1) DEFAULT '1' COMMENT '提问审核  1阻止提交  2关键词替换成*** ',
  `question_reply` varchar(255) DEFAULT NULL COMMENT '提问审核被阻止 默认回复',
  `reply_system` tinyint(1) DEFAULT '1' COMMENT '回复审核 系统词库审核   1开启 0关闭',
  `reply_deal` tinyint(1) DEFAULT '1' COMMENT '回复审核 系统词库审核 1阻止提交  2关键词替换成*** ',
  `reply_reply` varchar(255) DEFAULT NULL COMMENT '回复审核 被阻止 默认回复',
  `c_time` datetime DEFAULT NULL,
  `u_time` datetime DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC COMMENT='内容安全设置';

-- ----------------------------
-- Records of kt_base_content_security
-- ----------------------------

-- ----------------------------
-- Table structure for `kt_base_gpt_config`
-- ----------------------------
DROP TABLE IF EXISTS `kt_base_gpt_config`;
CREATE TABLE `kt_base_gpt_config` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `uid` int(11) DEFAULT NULL,
  `channel` tinyint(1) DEFAULT '1' COMMENT '渠道1.openai 2.api2d 3.文心一言 4.通义千问 5.昆仑天工 6.ChatGLM 7灵犀星火',
  `openai` text COMMENT 'openai',
  `api2d` text COMMENT 'api2d',
  `wxyy` text COMMENT '文心一言配置',
  `tyqw` text COMMENT '通义千文',
  `kltg` text COMMENT '昆仑天工',
  `chatglm` text COMMENT 'chatglm',
  `linkerai` text COMMENT '灵犀星火',
  `u_time` datetime DEFAULT NULL,
  `c_time` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;

-- ----------------------------
-- Records of kt_base_gpt_config
-- ----------------------------

-- ----------------------------
-- Table structure for `kt_base_imglib`
-- ----------------------------
DROP TABLE IF EXISTS `kt_base_imglib`;
CREATE TABLE `kt_base_imglib` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `uid` int(11) DEFAULT NULL COMMENT '账户id',
  `pid` int(11) DEFAULT NULL COMMENT '上级目录',
  `name` varchar(255) DEFAULT NULL COMMENT '名称',
  `url` varchar(255) DEFAULT NULL COMMENT '图片可访问地址',
  `oss_type` tinyint(1) DEFAULT NULL COMMENT '1: 本地 2:阿里云 3:腾讯云 4:七牛云',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `update_time` datetime DEFAULT NULL COMMENT '更新时间',
  `wid` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC COMMENT='上传的图片库列表';

-- ----------------------------
-- Records of kt_base_imglib
-- ----------------------------

-- ----------------------------
-- Table structure for `kt_base_kt_plugin`
-- ----------------------------
DROP TABLE IF EXISTS `kt_base_kt_plugin`;
CREATE TABLE `kt_base_kt_plugin` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL COMMENT '应用名称',
  `type` tinyint(1) DEFAULT '1' COMMENT '来源途径: 1:本地上传  2:远程安装',
  `version` varchar(20) DEFAULT NULL COMMENT '已安装版本号',
  `identifie` varchar(255) DEFAULT NULL COMMENT '应用标识',
  `file_name` varchar(255) DEFAULT NULL COMMENT '本地上传的文件名',
  `buytime` char(20) DEFAULT NULL COMMENT '应用购买时间',
  `can_update` tinyint(1) DEFAULT '0' COMMENT '是否可以更新',
  `new_version` varchar(255) DEFAULT NULL COMMENT '最新版本',
  `expire_date` datetime DEFAULT NULL COMMENT '免费升级到期时间',
  `orderbh` varchar(255) DEFAULT NULL COMMENT '订单编号',
  `plugin_id` int(11) DEFAULT NULL COMMENT '插件id',
  `desc` varchar(255) DEFAULT NULL COMMENT '描述',
  `create_time` datetime DEFAULT NULL COMMENT '上传时间',
  `update_time` datetime DEFAULT NULL COMMENT '更新时间',
  `code` varchar(255) DEFAULT NULL COMMENT 'code',
  `uid` int(11) DEFAULT NULL COMMENT '上传者id',
  `dir_name` varchar(255) DEFAULT NULL COMMENT '目录名',
  PRIMARY KEY (`id`),
  UNIQUE KEY `plugin_id` (`plugin_id`) USING BTREE COMMENT '插件id唯一'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='云市场插件记录表';

-- ----------------------------
-- Records of kt_base_kt_plugin
-- ----------------------------

-- ----------------------------
-- Table structure for `kt_base_ktconfig`
-- ----------------------------
DROP TABLE IF EXISTS `kt_base_ktconfig`;
CREATE TABLE `kt_base_ktconfig` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `uid` int(11) DEFAULT NULL,
  `key` varchar(255) DEFAULT NULL COMMENT '狂团配置  key',
  `secret` varchar(255) DEFAULT NULL COMMENT '狂团配置  secret',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of kt_base_ktconfig
-- ----------------------------

-- ----------------------------
-- Table structure for `kt_base_loginlog`
-- ----------------------------
DROP TABLE IF EXISTS `kt_base_loginlog`;
CREATE TABLE `kt_base_loginlog` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `wid` int(11) DEFAULT NULL COMMENT '账户id',
  `uip` char(50) DEFAULT NULL COMMENT 'ip',
  `admin` tinyint(1) DEFAULT NULL COMMENT '1: 代理登录  2:用户后台',
  `create_time` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=805 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of kt_base_loginlog
-- ----------------------------
INSERT INTO `kt_base_loginlog` VALUES ('799', '1', '36.99.250.216', '2', '2022-09-26 12:23:26');
INSERT INTO `kt_base_loginlog` VALUES ('800', '1', '36.99.250.197', '1', '2022-09-26 12:24:29');
INSERT INTO `kt_base_loginlog` VALUES ('801', '1', '153.35.52.224', '1', '2022-09-26 12:26:50');
INSERT INTO `kt_base_loginlog` VALUES ('802', '1', '153.35.52.191', '2', '2022-09-26 12:29:34');
INSERT INTO `kt_base_loginlog` VALUES ('803', '1', '61.141.64.131', '1', '2023-05-15 16:50:51');
INSERT INTO `kt_base_loginlog` VALUES ('804', '1', '61.141.64.131', '2', '2023-05-15 16:56:56');

-- ----------------------------
-- Table structure for `kt_base_market_app`
-- ----------------------------
DROP TABLE IF EXISTS `kt_base_market_app`;
CREATE TABLE `kt_base_market_app` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `uid` int(11) DEFAULT NULL COMMENT '代理id',
  `code` varchar(255) DEFAULT NULL COMMENT '应用编码(和配置文件相对应)',
  `name` varchar(255) DEFAULT NULL COMMENT '应用名称',
  `logo` varchar(255) DEFAULT NULL COMMENT '应用图标',
  `sort` int(11) DEFAULT NULL COMMENT '排序',
  `recom` tinyint(1) DEFAULT '0' COMMENT '推荐 0设为不推荐 1设为推荐',
  `try_days` int(11) DEFAULT NULL COMMENT '试用天数',
  `scene` varchar(255) DEFAULT NULL COMMENT '适用场景',
  `type` int(11) DEFAULT NULL COMMENT '分类',
  `version` varchar(255) DEFAULT NULL COMMENT '已安装版本号',
  `shelves` tinyint(1) DEFAULT '0' COMMENT '上下架 0下架 1上架',
  `label` varchar(255) DEFAULT NULL COMMENT '标签',
  `specs` longtext COMMENT '规格{duration:时长；duration_type:1:天2:月3:年；old_price:原价;price:售价}',
  `user_link` varchar(255) DEFAULT NULL COMMENT '用户后台首页',
  `admin_link` varchar(255) DEFAULT NULL COMMENT '管理后台首页',
  `app_type` tinyint(1) DEFAULT '1' COMMENT '应用类型: 1主应用 2插件 3工具; 注: 插件类型时, 文件夹名必须在主应用后面拼接 plugin, 如demo_plugin_demo',
  `describe` text COMMENT '描述',
  `pid` int(11) DEFAULT '0' COMMENT '非主引擎时 上级id, 最上级为0',
  `c_time` datetime DEFAULT NULL,
  `u_time` datetime DEFAULT NULL,
  `install_type` tinyint(1) DEFAULT '1' COMMENT '来源途径: 1:本地上传  2:远程安装',
  `file_name` varchar(255) DEFAULT NULL COMMENT '本地上传的文件名',
  `expire_date` datetime DEFAULT NULL COMMENT '免费升级到期时间',
  `orderbh` varchar(255) DEFAULT NULL COMMENT '订单编号',
  `target` tinyint(1) DEFAULT '2' COMMENT '首页打开方式1：当前窗口 2新窗口',
  `author` varchar(255) DEFAULT NULL COMMENT '作者',
  `has_applets` tinyint(1) DEFAULT '0' COMMENT '是否有小程序 0没有 1有',
  `applet_version` varchar(255) DEFAULT NULL COMMENT '小程序版本号',
  `draft_id` varchar(255) DEFAULT NULL COMMENT '草稿id',
  `template_id` varchar(255) DEFAULT NULL COMMENT '模板id',
  `template_update` tinyint(1) DEFAULT '0' COMMENT '最新版本是否已经请求模板更新 0未更新 1已更新',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=16 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of kt_base_market_app
-- ----------------------------
INSERT INTO `kt_base_market_app` VALUES ('13', '1', 'gptcms', 'GPTCMS', 'https://kt8logo.oss-cn-beijing.aliyuncs.com/app.png', '1', '0', '7', null, '6', '1.0.0', '1', '[]', '[{\"id\":1,\"duration\":\"1\",\"price\":\"1000\",\"old_price\":\"1000\",\"duration_type\":\"3\",\"index\":0}]', '/app/kt_ai', null, '1', 'GPTCMS', '0', '2023-05-15 17:07:39', '2023-05-15 17:07:39', '1', null, null, null, '1', '狂团', '0', null, null, null, '0');

-- ----------------------------
-- Table structure for `kt_base_market_type`
-- ----------------------------
DROP TABLE IF EXISTS `kt_base_market_type`;
CREATE TABLE `kt_base_market_type` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `uid` int(11) DEFAULT NULL COMMENT '代理id',
  `name` varchar(255) DEFAULT NULL COMMENT '分类名称',
  `sort` int(11) DEFAULT NULL COMMENT '排序',
  `level` int(11) DEFAULT '1' COMMENT '等级 1:一级分类 2：二级分类',
  `pid` int(11) DEFAULT NULL COMMENT '上级分类id',
  `c_time` datetime DEFAULT NULL,
  `u_time` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of kt_base_market_type
-- ----------------------------
INSERT INTO `kt_base_market_type` VALUES ('6', '1', '默认', '1', '1', '0', '2022-09-08 16:02:13', null);

-- ----------------------------
-- Table structure for `kt_base_pay_config`
-- ----------------------------
DROP TABLE IF EXISTS `kt_base_pay_config`;
CREATE TABLE `kt_base_pay_config` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `uid` int(11) DEFAULT NULL,
  `config` text COMMENT '支付配置',
  `type` varchar(100) DEFAULT NULL COMMENT '类型: wx 微信  ali 支付宝',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8 COMMENT='支付配置';

-- ----------------------------
-- Records of kt_base_pay_config
-- ----------------------------
INSERT INTO `kt_base_pay_config` VALUES ('3', '1', '1,1,1,1', 'wx');

-- ----------------------------
-- Table structure for `kt_base_recharge`
-- ----------------------------
DROP TABLE IF EXISTS `kt_base_recharge`;
CREATE TABLE `kt_base_recharge` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `bh` char(50) DEFAULT NULL COMMENT '编号',
  `order_bh` char(50) DEFAULT NULL COMMENT '订单编号',
  `wid` int(11) DEFAULT NULL COMMENT '账户id',
  `uip` char(50) DEFAULT NULL COMMENT '请求ip',
  `amount` decimal(10,2) DEFAULT NULL COMMENT '金额',
  `status` char(50) DEFAULT NULL COMMENT '状态',
  `alipayzt` char(50) DEFAULT NULL,
  `bz` varchar(250) NOT NULL,
  `ifok` int(11) DEFAULT NULL COMMENT '是否支付 1:已支付  0:未支付',
  `wxddbh` char(50) DEFAULT NULL COMMENT '微信订单编号',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `update_time` datetime DEFAULT NULL,
  `jyh` varchar(255) DEFAULT NULL COMMENT '交易单号',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=42 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of kt_base_recharge
-- ----------------------------
INSERT INTO `kt_base_recharge` VALUES ('41', '202209081604235125|1', '202209081604235125|1', '1', '', '1.00', '等待买家付款', null, '微信', '0', null, '2022-09-08 16:04:23', '2022-09-08 16:04:23', null);

-- ----------------------------
-- Table structure for `kt_base_register_set`
-- ----------------------------
DROP TABLE IF EXISTS `kt_base_register_set`;
CREATE TABLE `kt_base_register_set` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `uid` int(11) DEFAULT NULL COMMENT '账号id',
  `app_package_id` int(11) DEFAULT NULL COMMENT '注册默认开通应用套餐id',
  `package_specs_id` int(11) DEFAULT NULL COMMENT '注册默认开通应用套餐规格id',
  `c_time` datetime DEFAULT NULL,
  `u_time` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;

-- ----------------------------
-- Records of kt_base_register_set
-- ----------------------------

-- ----------------------------
-- Table structure for `kt_base_requests`
-- ----------------------------
DROP TABLE IF EXISTS `kt_base_requests`;
CREATE TABLE `kt_base_requests` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `service` varchar(255) DEFAULT NULL COMMENT '接口名称',
  `request_time` datetime DEFAULT NULL COMMENT '请求时间',
  `ip` varchar(60) DEFAULT NULL COMMENT '客户端ip',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;

-- ----------------------------
-- Records of kt_base_requests
-- ----------------------------
INSERT INTO `kt_base_requests` VALUES ('1', 'admin/system/appstore', '2023-05-15 16:52:12', '61.141.64.131');

-- ----------------------------
-- Table structure for `kt_base_robot`
-- ----------------------------
DROP TABLE IF EXISTS `kt_base_robot`;
CREATE TABLE `kt_base_robot` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `url` varchar(255) DEFAULT NULL COMMENT 'url连接地址',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;

-- ----------------------------
-- Records of kt_base_robot
-- ----------------------------

-- ----------------------------
-- Table structure for `kt_base_sms_config`
-- ----------------------------
DROP TABLE IF EXISTS `kt_base_sms_config`;
CREATE TABLE `kt_base_sms_config` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `uid` int(11) DEFAULT NULL COMMENT '代理id',
  `access_key_id` varchar(255) DEFAULT NULL COMMENT '阿里云短信key',
  `access_key_secret` varchar(255) DEFAULT NULL COMMENT '阿里云短信密钥',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COMMENT='阿里云短信配置';

-- ----------------------------
-- Records of kt_base_sms_config
-- ----------------------------
INSERT INTO `kt_base_sms_config` VALUES ('1', '1', '123', 'dcs');

-- ----------------------------
-- Table structure for `kt_base_sms_template`
-- ----------------------------
DROP TABLE IF EXISTS `kt_base_sms_template`;
CREATE TABLE `kt_base_sms_template` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `uid` int(11) DEFAULT NULL,
  `bh` varchar(20) DEFAULT NULL,
  `sign_name` varchar(100) DEFAULT NULL,
  `template_code` varchar(100) DEFAULT NULL,
  `content` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of kt_base_sms_template
-- ----------------------------
INSERT INTO `kt_base_sms_template` VALUES ('3', '1', '001', '1', '1', '1');

-- ----------------------------
-- Table structure for `kt_base_storage_config`
-- ----------------------------
DROP TABLE IF EXISTS `kt_base_storage_config`;
CREATE TABLE `kt_base_storage_config` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `uid` int(11) DEFAULT NULL COMMENT '账户id',
  `type` tinyint(1) DEFAULT '1' COMMENT '1: local本地 2:oss阿里云 3: cos 腾讯云 4: kodo七牛云',
  `oss_id` varchar(100) DEFAULT NULL COMMENT '阿里云 id',
  `oss_secret` varchar(100) DEFAULT NULL COMMENT '阿里云 secret',
  `oss_endpoint` varchar(100) DEFAULT NULL COMMENT '阿里云  访问域名',
  `oss_bucket` varchar(100) DEFAULT NULL COMMENT '阿里云bucket',
  `cos_secretId` varchar(100) DEFAULT NULL COMMENT '腾讯云 id',
  `cos_secretKey` varchar(100) DEFAULT NULL COMMENT '腾讯云 key',
  `cos_bucket` varchar(100) DEFAULT NULL COMMENT '腾讯云 bucket',
  `cos_endpoint` varchar(100) DEFAULT NULL COMMENT '腾讯云 endpoint',
  `kodo_key` varchar(100) DEFAULT NULL COMMENT '七牛云 key',
  `kodo_secret` varchar(100) DEFAULT NULL COMMENT '七牛云 secret',
  `kodo_domain` varchar(100) DEFAULT NULL COMMENT '七牛云 domain',
  `kodo_bucket` varchar(100) DEFAULT NULL COMMENT '七牛云 bucket',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `update_time` datetime DEFAULT NULL COMMENT '更新时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=16 DEFAULT CHARSET=utf8 COMMENT='云存储配置';

-- ----------------------------
-- Records of kt_base_storage_config
-- ----------------------------

-- ----------------------------
-- Table structure for `kt_base_tencentai_config`
-- ----------------------------
DROP TABLE IF EXISTS `kt_base_tencentai_config`;
CREATE TABLE `kt_base_tencentai_config` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `uid` int(11) DEFAULT NULL,
  `secret_id` varchar(255) DEFAULT NULL,
  `secret_key` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;

-- ----------------------------
-- Records of kt_base_tencentai_config
-- ----------------------------

-- ----------------------------
-- Table structure for `kt_base_user`
-- ----------------------------
DROP TABLE IF EXISTS `kt_base_user`;
CREATE TABLE `kt_base_user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `un` varchar(200) NOT NULL COMMENT '用户名',
  `pwd` varchar(200) NOT NULL COMMENT '密码',
  `email` varchar(50) DEFAULT NULL COMMENT '邮箱',
  `telephone` varchar(20) DEFAULT NULL COMMENT '电话',
  `qq` int(11) DEFAULT NULL COMMENT 'qq',
  `logtimes` bigint(20) DEFAULT '0' COMMENT '登录次数 每登一次加一',
  `isadmin` tinyint(1) DEFAULT '1' COMMENT '1: 管理账户 2:子用户',
  `level_id` tinyint(1) DEFAULT '1' COMMENT '用户级别',
  `mendtime` datetime DEFAULT NULL COMMENT '到期时间',
  `agid` int(11) unsigned DEFAULT '0' COMMENT '上级代理id',
  `isstop` tinyint(1) DEFAULT '1' COMMENT '账号状态: 1正常   0:停用，2待审核',
  `xiane` smallint(6) DEFAULT '1' COMMENT '账户限额',
  `balance` decimal(10,2) DEFAULT NULL COMMENT '账户余额',
  `alias` varchar(255) DEFAULT NULL COMMENT '别名',
  `token` varchar(255) DEFAULT NULL COMMENT 'token',
  `expire_time` char(20) DEFAULT '' COMMENT 'token过期时间',
  `create_time` datetime DEFAULT NULL COMMENT '注册时间',
  `update_time` datetime DEFAULT NULL COMMENT '更新时间',
  `status` int(10) DEFAULT '1' COMMENT '1为正常，0为禁用，2为作废',
  `remark` varchar(255) DEFAULT NULL COMMENT '备注信息',
  `contacts` varchar(255) DEFAULT NULL COMMENT '联系人',
  `lasttime` datetime DEFAULT NULL COMMENT '最后一次登陆时间',
  `wxopenid` varchar(255) DEFAULT NULL COMMENT '微信openid',
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE KEY `user_un` (`un`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=19 DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC COMMENT='系统用户（客户）表';

-- ----------------------------
-- Records of kt_base_user
-- ----------------------------
INSERT INTO `kt_base_user` VALUES ('1', '123456', 'e10adc3949ba59abbe56e057f20f883e', null, '', null, '139', '2', '2', '2022-10-31 00:00:00', '1', '1', '1', '9997.00', '33', '71cab3d4-f2fe-11ed-b6af-00163e003301', '1684746514', '2020-08-01 18:09:54', '2022-09-26 12:32:04', '1', null, '123', '2023-05-15 16:56:56', null);

-- ----------------------------
-- Table structure for `kt_base_user_appauth`
-- ----------------------------
DROP TABLE IF EXISTS `kt_base_user_appauth`;
CREATE TABLE `kt_base_user_appauth` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `wid` int(11) DEFAULT NULL COMMENT '账户id',
  `title` varchar(255) DEFAULT NULL COMMENT '应用标题',
  `code` varchar(255) DEFAULT NULL COMMENT '应用标识',
  `create_time` datetime DEFAULT NULL,
  `mend_time` datetime DEFAULT NULL COMMENT '到期时间',
  `set_meal` int(10) DEFAULT NULL COMMENT '对应表套餐id',
  `name` varchar(255) DEFAULT NULL COMMENT '应用名称',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8 COMMENT='用户应用权限';

-- ----------------------------
-- Records of kt_base_user_appauth
-- ----------------------------

-- ----------------------------
-- Table structure for `kt_base_user_openapp`
-- ----------------------------
DROP TABLE IF EXISTS `kt_base_user_openapp`;
CREATE TABLE `kt_base_user_openapp` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `wid` int(11) DEFAULT NULL COMMENT '账户id',
  `title` varchar(255) DEFAULT NULL COMMENT '应用标题',
  `code` varchar(255) DEFAULT NULL COMMENT '应用标识',
  `mend_time` datetime DEFAULT NULL COMMENT '到期时间',
  `name` varchar(255) DEFAULT NULL COMMENT '应用名称',
  `create_time` datetime DEFAULT NULL,
  `update_time` datetime DEFAULT NULL,
  `app_id` int(11) DEFAULT NULL COMMENT '引擎id',
  `logo` varchar(255) DEFAULT NULL COMMENT 'logo',
  `version` varchar(255) DEFAULT NULL COMMENT '版本',
  `sequence` int(5) DEFAULT NULL COMMENT '序号 越大越靠前',
  `self_title` varchar(255) DEFAULT NULL COMMENT '自定义标题',
  `pid` int(11) DEFAULT '0' COMMENT '上级id',
  `status` tinyint(1) DEFAULT '1' COMMENT '1启用 0停用',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=14 DEFAULT CHARSET=utf8 COMMENT='用户购买的引擎';

-- ----------------------------
-- Records of kt_base_user_openapp
-- ----------------------------
INSERT INTO `kt_base_user_openapp` VALUES ('13', '1', null, 'gptcms', '2024-05-15 16:56:42', 'GPTCMS', '2023-05-15 16:56:42', '2023-05-15 16:56:42', '13', 'https://kt8logo.oss-cn-beijing.aliyuncs.com/app.png', '1.0.0', null, null, '0', '1');

-- ----------------------------
-- Table structure for `kt_base_user_order`
-- ----------------------------
DROP TABLE IF EXISTS `kt_base_user_order`;
CREATE TABLE `kt_base_user_order` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `wid` int(11) DEFAULT NULL,
  `content` varchar(255) DEFAULT NULL COMMENT '内容',
  `app_id` int(11) DEFAULT NULL COMMENT '应用id',
  `price` decimal(10,2) DEFAULT NULL COMMENT '价格',
  `create_time` datetime DEFAULT NULL,
  `openapp_id` int(11) DEFAULT NULL COMMENT '购买应用id',
  `status` tinyint(1) DEFAULT NULL COMMENT '1:待支付  2:支付成功 3:支付失败',
  `balance_pay` decimal(10,2) DEFAULT NULL COMMENT '余额支付金额',
  `specs_id` int(1) DEFAULT NULL COMMENT '规格id',
  `bh` varchar(255) DEFAULT NULL COMMENT '订单编号',
  `specs_content` text COMMENT '规格内容',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=64 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of kt_base_user_order
-- ----------------------------
INSERT INTO `kt_base_user_order` VALUES ('55', '1', null, '11', '1.00', '2022-09-08 16:04:23', null, '1', null, '1', '202209081604235125|1', '{\"id\":1,\"duration\":\"1\",\"price\":\"1\",\"old_price\":\"1000\",\"duration_type\":\"3\",\"index\":0}');
INSERT INTO `kt_base_user_order` VALUES ('56', '1', null, '11', '1.00', '2022-09-08 16:04:45', null, '1', null, '1', '202209081604457371|1', '{\"id\":1,\"duration\":\"1\",\"price\":\"1\",\"old_price\":\"1000\",\"duration_type\":\"3\",\"index\":0}');
INSERT INTO `kt_base_user_order` VALUES ('57', '1', '用户试用', '11', '0.00', '2022-09-08 16:05:07', '12', '2', null, null, null, null);
INSERT INTO `kt_base_user_order` VALUES ('58', '1', null, '11', '1.00', '2022-09-08 16:14:59', null, '1', null, '1', '202209081614598412|1', '{\"id\":1,\"duration\":\"1\",\"price\":\"1\",\"old_price\":\"1000\",\"duration_type\":\"3\",\"index\":0}');
INSERT INTO `kt_base_user_order` VALUES ('59', '1', null, '11', '1.00', '2022-09-08 16:21:44', null, '1', null, '1', '202209081621447164|1', '{\"id\":1,\"duration\":\"1\",\"price\":\"1\",\"old_price\":\"1000\",\"duration_type\":\"3\",\"index\":0}');
INSERT INTO `kt_base_user_order` VALUES ('60', '1', null, '11', '1.00', '2022-09-08 16:21:47', null, '1', null, '1', '202209081621471603|1', '{\"id\":1,\"duration\":\"1\",\"price\":\"1\",\"old_price\":\"1000\",\"duration_type\":\"3\",\"index\":0}');
INSERT INTO `kt_base_user_order` VALUES ('61', '1', '用户购买规格时长1年的套餐', '11', '1.00', '2022-09-08 16:23:27', null, '2', null, '1', '202209081623276569|1', '{\"id\":1,\"duration\":\"1\",\"price\":\"1\",\"old_price\":\"1000\",\"duration_type\":\"3\",\"index\":0}');
INSERT INTO `kt_base_user_order` VALUES ('62', '1', '用户购买规格时长1年的套餐', '11', '1.00', '2022-09-08 16:24:42', null, '2', null, '1', '202209081624429084|1', '{\"id\":1,\"duration\":\"1\",\"price\":\"1\",\"old_price\":\"1000\",\"duration_type\":\"3\",\"index\":0}');
INSERT INTO `kt_base_user_order` VALUES ('63', '1', '用户购买规格时长1年的套餐', '11', '1.00', '2022-09-26 12:32:04', null, '2', null, '1', '202209261232049885|1', '{\"id\":1,\"duration\":\"1\",\"price\":\"1\",\"old_price\":\"1000\",\"duration_type\":\"3\",\"index\":0}');

-- ----------------------------
-- Table structure for `kt_base_user_package_recode`
-- ----------------------------
DROP TABLE IF EXISTS `kt_base_user_package_recode`;
CREATE TABLE `kt_base_user_package_recode` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `uid` int(11) DEFAULT NULL,
  `wid` int(11) DEFAULT NULL,
  `package_id` int(11) DEFAULT NULL COMMENT '套餐id',
  `specs_id` int(11) DEFAULT NULL,
  `app` text COMMENT '所有应用',
  `specs_content` text COMMENT '规格内容',
  `create_time` datetime DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC COMMENT='套餐开通记录';

-- ----------------------------
-- Records of kt_base_user_package_recode
-- ----------------------------

-- ----------------------------
-- Table structure for `kt_base_user_recharge_record`
-- ----------------------------
DROP TABLE IF EXISTS `kt_base_user_recharge_record`;
CREATE TABLE `kt_base_user_recharge_record` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL COMMENT 'base_user',
  `money` double NOT NULL COMMENT '充值金额',
  `recharge_time` datetime NOT NULL COMMENT '充值时间',
  `out_trade_no` varchar(255) NOT NULL COMMENT '订单号',
  `type` int(10) DEFAULT '1' COMMENT '1为充值，2为扣除，3为支付',
  `status` int(11) DEFAULT '1',
  `agid` int(10) DEFAULT NULL COMMENT '充值方或者扣除方id',
  `remark` varchar(255) DEFAULT NULL COMMENT '备注',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=28 DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC COMMENT='用户充值记录';

-- ----------------------------
-- Records of kt_base_user_recharge_record
-- ----------------------------
INSERT INTO `kt_base_user_recharge_record` VALUES ('27', '1', '10000', '2022-09-08 16:04:38', '20220908160438166262427857771', '1', '1', '1', '');

-- ----------------------------
-- Table structure for `kt_base_user_template`
-- ----------------------------
DROP TABLE IF EXISTS `kt_base_user_template`;
CREATE TABLE `kt_base_user_template` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `uid` int(11) DEFAULT NULL COMMENT '代理id',
  `code` varchar(255) DEFAULT NULL COMMENT '应用标识',
  `ctime` datetime DEFAULT NULL,
  `utime` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;

-- ----------------------------
-- Records of kt_base_user_template
-- ----------------------------

-- ----------------------------
-- Table structure for `kt_base_wx_open_confing`
-- ----------------------------
DROP TABLE IF EXISTS `kt_base_wx_open_confing`;
CREATE TABLE `kt_base_wx_open_confing` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `appid` varchar(255) DEFAULT NULL,
  `appsecret` varchar(255) DEFAULT NULL,
  `token` varchar(255) DEFAULT NULL,
  `key` varchar(255) DEFAULT NULL,
  `ctime` datetime DEFAULT NULL,
  `pid` int(10) DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;

-- ----------------------------
-- Records of kt_base_wx_open_confing
-- ----------------------------

-- ----------------------------
-- Table structure for `kt_base_wxgzh`
-- ----------------------------
DROP TABLE IF EXISTS `kt_base_wxgzh`;
CREATE TABLE `kt_base_wxgzh` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `uid` int(10) DEFAULT NULL COMMENT '代理id',
  `appid` varchar(255) DEFAULT NULL COMMENT '公众号appid',
  `appsecret` varchar(255) DEFAULT NULL COMMENT '公众号appsecret',
  `token` varchar(255) DEFAULT NULL COMMENT '令牌',
  `message_key` varchar(255) DEFAULT NULL COMMENT '消息加解密密钥',
  `message_mode` tinyint(1) DEFAULT '1' COMMENT '1:明文模式 2：兼容模式3：安全模式',
  `type` tinyint(1) DEFAULT '1' COMMENT '1手动配置 2扫码授权',
  `create_time` datetime DEFAULT NULL,
  `update_time` datetime DEFAULT NULL,
  `original_id` varchar(255) DEFAULT NULL COMMENT '公众号原始id',
  `switch` tinyint(1) DEFAULT '0' COMMENT '开关 0关 1开',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;

-- ----------------------------
-- Records of kt_base_wxgzh
-- ----------------------------

-- ----------------------------
-- Table structure for `kt_base_wxgzh_random`
-- ----------------------------
DROP TABLE IF EXISTS `kt_base_wxgzh_random`;
CREATE TABLE `kt_base_wxgzh_random` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `uid` int(10) DEFAULT NULL COMMENT '代理id',
  `appid` varchar(255) DEFAULT NULL COMMENT '微信公众平台appid',
  `openid` varchar(255) DEFAULT NULL COMMENT '访问的客户openid',
  `type` varchar(255) DEFAULT NULL COMMENT 'login：登录 bind：绑定',
  `random` varchar(255) DEFAULT NULL COMMENT '生成所携带的随机数',
  `un` varchar(255) DEFAULT NULL COMMENT '初始账号',
  `pwd` varchar(255) DEFAULT NULL COMMENT '初始密码',
  `code` varchar(255) DEFAULT NULL COMMENT '二维码图片',
  `ctime` datetime DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;

-- ----------------------------
-- Records of kt_base_wxgzh_random
-- ----------------------------

-- ----------------------------
-- Table structure for `kt_chat_msg`
-- ----------------------------
DROP TABLE IF EXISTS `kt_chat_msg`;
CREATE TABLE `kt_chat_msg` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `uip` varchar(255) DEFAULT NULL COMMENT '客户端ip',
  `group_id` int(11) DEFAULT NULL COMMENT '分组id',
  `message` text COMMENT '用户发出内容',
  `response` text COMMENT '助手回复内容',
  `c_time` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;

-- ----------------------------
-- Records of kt_chat_msg
-- ----------------------------

-- ----------------------------
-- Table structure for `kt_chat_msg_group`
-- ----------------------------
DROP TABLE IF EXISTS `kt_chat_msg_group`;
CREATE TABLE `kt_chat_msg_group` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `uip` varchar(255) DEFAULT NULL COMMENT '客户端ip',
  `name` varchar(255) DEFAULT NULL COMMENT '分组名称',
  `c_time` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;

-- ----------------------------
-- Records of kt_chat_msg_group
-- ----------------------------

-- ----------------------------
-- Table structure for `kt_gptcms_aliai_config`
-- ----------------------------
DROP TABLE IF EXISTS `kt_gptcms_aliai_config`;
CREATE TABLE `kt_gptcms_aliai_config` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `wid` int(11) DEFAULT NULL,
  `accesskey_id` varchar(255) DEFAULT NULL,
  `accesskey_secret` varchar(255) DEFAULT NULL,
  `region` varchar(255) DEFAULT NULL,
  `appkey` varchar(255) DEFAULT NULL,
  `status` tinyint(4) DEFAULT '0',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;

-- ----------------------------
-- Records of kt_gptcms_aliai_config
-- ----------------------------

-- ----------------------------
-- Table structure for `kt_gptcms_alipay_config`
-- ----------------------------
DROP TABLE IF EXISTS `kt_gptcms_alipay_config`;
CREATE TABLE `kt_gptcms_alipay_config` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `wid` int(10) DEFAULT NULL,
  `app_id` varchar(255) DEFAULT NULL COMMENT 'appid',
  `merchant_private_key` text COMMENT '应用私钥',
  `alipay_public_key` text COMMENT '支付宝公钥',
  `status` int(10) DEFAULT '0' COMMENT '1为开启，0为关闭',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;

-- ----------------------------
-- Records of kt_gptcms_alipay_config
-- ----------------------------

-- ----------------------------
-- Table structure for `kt_gptcms_api_employ`
-- ----------------------------
DROP TABLE IF EXISTS `kt_gptcms_api_employ`;
CREATE TABLE `kt_gptcms_api_employ` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `wid` int(11) DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL COMMENT '应用名称',
  `api_key` varchar(255) DEFAULT NULL COMMENT 'API_KEY',
  `status` tinyint(1) DEFAULT '1' COMMENT '状态0关闭 1开启',
  `ctime` datetime DEFAULT NULL,
  `utime` datetime DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;

-- ----------------------------
-- Records of kt_gptcms_api_employ
-- ----------------------------

-- ----------------------------
-- Table structure for `kt_gptcms_baiduai_config`
-- ----------------------------
DROP TABLE IF EXISTS `kt_gptcms_baiduai_config`;
CREATE TABLE `kt_gptcms_baiduai_config` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `wid` int(11) DEFAULT NULL,
  `appid` int(11) DEFAULT NULL,
  `apikey` varchar(255) DEFAULT NULL,
  `secretkey` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;

-- ----------------------------
-- Records of kt_gptcms_baiduai_config
-- ----------------------------

-- ----------------------------
-- Table structure for `kt_gptcms_card`
-- ----------------------------
DROP TABLE IF EXISTS `kt_gptcms_card`;
CREATE TABLE `kt_gptcms_card` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `wid` int(10) DEFAULT NULL,
  `type` int(10) DEFAULT NULL COMMENT '1为对话次，3为vip时长',
  `size` int(10) DEFAULT NULL COMMENT '与类型为对应',
  `size_num` int(10) DEFAULT NULL COMMENT '时长或条数数量',
  `remark` varchar(255) DEFAULT NULL COMMENT '备注信息',
  `amount` int(10) DEFAULT NULL COMMENT '生成卡密数量',
  `ctime` datetime DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;

-- ----------------------------
-- Records of kt_gptcms_card
-- ----------------------------

-- ----------------------------
-- Table structure for `kt_gptcms_card_detail`
-- ----------------------------
DROP TABLE IF EXISTS `kt_gptcms_card_detail`;
CREATE TABLE `kt_gptcms_card_detail` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `pid` int(10) DEFAULT NULL,
  `code` varchar(255) DEFAULT NULL COMMENT '卡密',
  `time` datetime DEFAULT NULL COMMENT '使用时间',
  `user` int(10) DEFAULT NULL COMMENT '使用者',
  `status` int(10) DEFAULT '0' COMMENT '0未使用，1为已使用',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;

-- ----------------------------
-- Records of kt_gptcms_card_detail
-- ----------------------------

-- ----------------------------
-- Table structure for `kt_gptcms_chat_msg`
-- ----------------------------
DROP TABLE IF EXISTS `kt_gptcms_chat_msg`;
CREATE TABLE `kt_gptcms_chat_msg` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `wid` int(11) DEFAULT NULL COMMENT '用户id',
  `common_id` int(11) DEFAULT NULL COMMENT '终端用户id',
  `group_id` int(11) DEFAULT '0',
  `un_message` text CHARACTER SET utf8mb4 COMMENT '用户发出内容(未过滤)',
  `message` text CHARACTER SET utf8mb4 COMMENT '用户发出内容(已过滤)',
  `un_response` text CHARACTER SET utf8mb4 COMMENT '助手回复内容(未过滤)',
  `response` text CHARACTER SET utf8mb4 COMMENT '助手回复内容(已过滤)',
  `total_tokens` int(11) DEFAULT NULL COMMENT '发出+回复字符串的总长度',
  `c_time` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=130 DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;

-- ----------------------------
-- Records of kt_gptcms_chat_msg
-- ----------------------------

-- ----------------------------
-- Table structure for `kt_gptcms_chat_msg_group`
-- ----------------------------
DROP TABLE IF EXISTS `kt_gptcms_chat_msg_group`;
CREATE TABLE `kt_gptcms_chat_msg_group` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `wid` int(11) DEFAULT NULL COMMENT '用户id',
  `common_id` int(11) DEFAULT NULL COMMENT '客户端用户id',
  `name` varchar(255) DEFAULT NULL COMMENT '分组名称',
  `c_time` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;

-- ----------------------------
-- Records of kt_gptcms_chat_msg_group
-- ----------------------------

-- ----------------------------
-- Table structure for `kt_gptcms_chatmodel_set`
-- ----------------------------
DROP TABLE IF EXISTS `kt_gptcms_chatmodel_set`;
CREATE TABLE `kt_gptcms_chatmodel_set` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `wid` int(11) DEFAULT NULL,
  `status` tinyint(1) DEFAULT '0' COMMENT '1开启  0关闭',
  `gpt35` text COMMENT 'GPT3.5',
  `gpt4` text COMMENT 'Gpt4',
  `linkerai` text COMMENT '灵犀星火',
  `api2d35` text COMMENT 'api2d3.5',
  `api2d4` varchar(255) DEFAULT NULL COMMENT 'api2d4',
  `wxyy` text COMMENT '文心一言',
  `c_time` datetime DEFAULT NULL,
  `u_time` datetime DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC COMMENT='对话模型设置';

-- ----------------------------
-- Records of kt_gptcms_chatmodel_set
-- ----------------------------

-- ----------------------------
-- Table structure for `kt_gptcms_cmodel`
-- ----------------------------
DROP TABLE IF EXISTS `kt_gptcms_cmodel`;
CREATE TABLE `kt_gptcms_cmodel` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `wid` int(11) DEFAULT NULL,
  `title` varchar(255) DEFAULT NULL COMMENT '名称',
  `tp_url` varchar(255) DEFAULT NULL COMMENT '图片url',
  `desc` varchar(255) DEFAULT NULL COMMENT '描述',
  `bz` varchar(255) DEFAULT NULL COMMENT '备注',
  `xh` int(11) DEFAULT '0' COMMENT '序号 越大越靠前',
  `vip_status` tinyint(1) DEFAULT '0' COMMENT 'vip是否可使用 1可以使用 0不能使用',
  `status` tinyint(1) DEFAULT '1' COMMENT '状态 1正常 0暂停使用',
  `c_time` datetime DEFAULT NULL,
  `u_time` datetime DEFAULT NULL,
  `classify_id` int(5) DEFAULT NULL COMMENT '分类id',
  `content` text COMMENT '模型内容',
  `hint_content` text COMMENT '对话框提示文字',
  `defalut_question` text COMMENT '默认问题',
  `defalut_reply` text COMMENT '默认回复',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=52 DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC COMMENT='创作模型';

-- ----------------------------
-- Records of kt_gptcms_cmodel
-- ----------------------------
INSERT INTO `kt_gptcms_cmodel` VALUES ('36', '1', '文案作家', 'http://gptcms.xcx0.com/static/gptcms/img/cz1.png', '生成各种风格高端文案', null, '16', '0', '1', '2023-05-15 16:57:14', '2023-05-15 16:57:14', '5', '我想让你成为一个非常熟练的高端文案作家，以至于能超越其他作家。你的任务是根据提供的大纲：[PROMPT]。写一篇文章。在新段落中为大纲中的每一行写内容，包括使用相关关键字的副标题。所有输出均应为简体中文，且必须为100%的人类书写风格，并修复语法错误。使用[TARGETLANGGE]书写。', '您好，请告诉我文案大纲和语言，我帮您完成创作！', null, null);
INSERT INTO `kt_gptcms_cmodel` VALUES ('37', '1', '知乎问答', 'http://gptcms.xcx0.com/static/gptcms/img/cz2.png', '生成知乎干货式问答	', null, '15', '0', '1', '2023-05-15 16:57:14', '2023-05-15 16:57:14', '5', '我想让你根据[PROMPT] 帮我生成知乎问答', '您好，我可以帮您生成知乎问答，请告诉我您想要的关键词、字数、风格（如正式、专业、幽默、热情）等！', null, null);
INSERT INTO `kt_gptcms_cmodel` VALUES ('38', '1', '个性签名', 'http://gptcms.xcx0.com/static/gptcms/img/cz3.png', '一键生成各种社交个性签名', null, '14', '0', '1', '2023-05-15 16:57:14', '2023-05-15 16:57:14', '5', '我想让你根据[PROMPT] 帮我生成社交个性签名', '您好，我可以帮您生成社交个性签名，请告诉我您想要的社交平台、关键词、字数等！', null, null);
INSERT INTO `kt_gptcms_cmodel` VALUES ('39', '1', '考研题目', 'http://gptcms.xcx0.com/static/gptcms/img/cz4.png', '针对考研生成各个学科题目', null, '13', '0', '1', '2023-05-15 16:57:14', '2023-05-15 16:57:14', '5', '我想让你根据[PROMPT]帮我针对考研生成各个学科题目', '您好，请告诉我您想要的科目和题目类型，及关键词', null, null);
INSERT INTO `kt_gptcms_cmodel` VALUES ('40', '1', '合同模板', 'http://gptcms.xcx0.com/static/gptcms/img/cz5.png', '生成各种类型合同模板', null, '12', '0', '1', '2023-05-15 16:57:14', '2023-05-15 16:57:14', '5', '我想让你根据[PROMPT]帮我生成合同模板', '您好，请告诉我您想要的合同类型（如劳动合同、租赁合同、买卖合同等）及主题描述', null, null);
INSERT INTO `kt_gptcms_cmodel` VALUES ('41', '1', '作文', 'http://gptcms.xcx0.com/static/gptcms/img/cz6.png', '一键生成学生各阶段写作内容', null, '11', '0', '1', '2023-05-15 16:57:14', '2023-05-15 16:57:14', '5', '我想让你根据[PROMPT]帮我生成作文', '您好，请告诉我您想要的作文标题、关键字、字数等必要信息', null, null);
INSERT INTO `kt_gptcms_cmodel` VALUES ('42', '1', '短视频', 'http://gptcms.xcx0.com/static/gptcms/img/cz7.png', '生成短视频大纲、口播稿、灵感等内容', null, '10', '0', '1', '2023-05-15 16:57:14', '2023-05-15 16:57:14', '5', '我想让你根据[PROMPT]帮我生成短视频大纲、口播稿、灵感等内容', '您好，请告诉我您想要的短视频类型（如脚本大纲、口播稿、内容灵感、标题等），关键词/主题，风格、字数等必要信息', null, null);
INSERT INTO `kt_gptcms_cmodel` VALUES ('43', '1', '诗歌情诗', 'http://gptcms.xcx0.com/static/gptcms/img/cz8.png', '一键生成各种类型的精美诗词', null, '9', '0', '1', '2023-05-15 16:57:14', '2023-05-15 16:57:14', '5', '我想让你根据[PROMPT]帮我生成各种类型的精美诗词', '您好，请告诉我您想要的生成的诗词类别（如爱情，友情，怀古等），关键词/主题，形式（如古诗、楚辞、七言古诗等', null, null);
INSERT INTO `kt_gptcms_cmodel` VALUES ('44', '1', '周报', 'http://gptcms.xcx0.com/static/gptcms/img/cz9.png', '一键汇报工作成果', null, '8', '0', '1', '2023-05-15 16:57:14', '2023-05-15 16:57:14', '5', '我想让你根据[PROMPT]帮我生成工作周报', '您好，请描述您的工作内容，及关键词、想要的字数等', null, null);
INSERT INTO `kt_gptcms_cmodel` VALUES ('45', '1', '日程计划', 'http://gptcms.xcx0.com/static/gptcms/img/cz10.png', '为您制定完美计划', null, '7', '0', '1', '2023-05-15 16:57:14', '2023-05-15 16:57:14', '5', '我想让你根据[PROMPT]帮我制定日程计划', '您好，请告诉我您的日程主题、周期时间等必要信息', null, null);
INSERT INTO `kt_gptcms_cmodel` VALUES ('46', '1', '电商', 'http://gptcms.xcx0.com/static/gptcms/img/cz11.png', '为电商产品生产各种内容，如SEO优化等', null, '6', '0', '1', '2023-05-15 16:57:14', '2023-05-15 16:57:14', '5', '我想让你根据[PROMPT]帮我的电商产品生产各种内容', '您好，请告诉我您的产品名称、卖点、风格（如正式、专业、幽默等）、字数等', null, null);
INSERT INTO `kt_gptcms_cmodel` VALUES ('47', '1', '起名字', 'http://gptcms.xcx0.com/static/gptcms/img/cz12.png', '针对各种对象起适合的好名字', null, '5', '0', '1', '2023-05-15 16:57:14', '2023-05-15 16:57:14', '5', '我想让你根据[PROMPT]帮我起名字', '您好，请告诉我相关关键词、字数或其他信息', null, null);
INSERT INTO `kt_gptcms_cmodel` VALUES ('48', '1', 'SWOT分析法', 'http://gptcms.xcx0.com/static/gptcms/img/cz13.png', '使用SWOT分析法满足您的需求', null, '4', '0', '1', '2023-05-15 16:57:14', '2023-05-15 16:57:14', '5', '我想让你根据[PROMPT]用SWOT分析法满足我的需求', '您好，请告诉我您想要的分析对象及优势/劣势、机会/威胁、字数等必要信息', null, null);
INSERT INTO `kt_gptcms_cmodel` VALUES ('49', '1', '梦境分析', 'http://gptcms.xcx0.com/static/gptcms/img/cz14.png', '为您的梦境进行解梦', null, '3', '0', '1', '2023-05-15 16:57:14', '2023-05-15 16:57:14', '5', '我想让你根据[PROMPT]帮我分析梦境', '您好，请告诉我您梦到了什么？', null, null);
INSERT INTO `kt_gptcms_cmodel` VALUES ('50', '1', '回复老板', 'http://gptcms.xcx0.com/static/gptcms/img/cz15.png', '一键生成各种场景下回复老板文案', null, '2', '0', '1', '2023-05-15 16:57:14', '2023-05-15 16:57:14', '5', '我想让你根据[PROMPT]帮我生成各种场景下回复老板文案', '您好，请告诉我老板给您发的信息描述，及回复内容关键词、字数等信息', null, null);
INSERT INTO `kt_gptcms_cmodel` VALUES ('51', '1', '多语言翻译', 'http://gptcms.xcx0.com/static/gptcms/img/cz16.png', '一键帮您翻译成想要的语言', null, '1', '0', '1', '2023-05-15 16:57:14', '2023-05-15 16:57:14', '5', '我想让你根据[PROMPT]把我输入的内容翻译成我想要的语言', '您好，请提供您需要翻译的内容及目标语言', null, null);

-- ----------------------------
-- Table structure for `kt_gptcms_cmodel_classify`
-- ----------------------------
DROP TABLE IF EXISTS `kt_gptcms_cmodel_classify`;
CREATE TABLE `kt_gptcms_cmodel_classify` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `wid` int(11) DEFAULT NULL,
  `title` varchar(255) DEFAULT NULL,
  `sort` int(11) DEFAULT '0' COMMENT '排序',
  `c_time` datetime DEFAULT NULL,
  `u_time` datetime DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;

-- ----------------------------
-- Records of kt_gptcms_cmodel_classify
-- ----------------------------
INSERT INTO `kt_gptcms_cmodel_classify` VALUES ('5', '1', '默认', '0', '2023-05-15 16:57:14', '2023-05-15 16:57:14');

-- ----------------------------
-- Table structure for `kt_gptcms_common_user`
-- ----------------------------
DROP TABLE IF EXISTS `kt_gptcms_common_user`;
CREATE TABLE `kt_gptcms_common_user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `wid` int(11) DEFAULT NULL COMMENT '系统用户id',
  `type` varchar(50) DEFAULT NULL COMMENT 'xcx 微信小程序 h5 H5 wx 微信',
  `parent` int(11) DEFAULT '0' COMMENT '所属上级',
  `level` int(11) DEFAULT '1' COMMENT '级别',
  `residue_degree` int(11) DEFAULT '0' COMMENT '剩余条数',
  `vip_expire` datetime DEFAULT NULL COMMENT 'vip到期时间',
  `vip_open` datetime DEFAULT NULL COMMENT 'vip开通时间',
  `money` decimal(10,2) DEFAULT '0.00' COMMENT '余额',
  `mobile` varchar(255) DEFAULT NULL COMMENT '手机号',
  `nickname` varchar(255) DEFAULT NULL COMMENT '昵称',
  `headimgurl` varchar(255) DEFAULT NULL COMMENT '头像',
  `account` varchar(255) DEFAULT NULL COMMENT '账号',
  `password` varchar(255) DEFAULT NULL COMMENT '密码',
  `unionid` varchar(255) DEFAULT NULL COMMENT '开放平台id',
  `token` varchar(255) DEFAULT NULL COMMENT 'token',
  `expire_time` int(11) DEFAULT NULL COMMENT 'token过期时间',
  `xcx_token` varchar(255) DEFAULT NULL COMMENT '微信小程序token',
  `status` tinyint(1) DEFAULT '1' COMMENT '状态 1正常 0停用',
  `bz` varchar(255) DEFAULT NULL COMMENT '备注',
  `c_time` datetime DEFAULT NULL,
  `u_time` datetime DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=62 DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;

-- ----------------------------
-- Records of kt_gptcms_common_user
-- ----------------------------

-- ----------------------------
-- Table structure for `kt_gptcms_content_security`
-- ----------------------------
DROP TABLE IF EXISTS `kt_gptcms_content_security`;
CREATE TABLE `kt_gptcms_content_security` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `wid` int(11) DEFAULT NULL,
  `question_baiduai` tinyint(1) DEFAULT '1' COMMENT '提问审核 百度ai文本审核开关 1开启 0关闭',
  `reply_baiduai` tinyint(1) DEFAULT '1' COMMENT '回复审核 百度ai文本审核开关  1开启 0关闭',
  `c_time` datetime DEFAULT NULL,
  `u_time` datetime DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC COMMENT='内容安全设置';

-- ----------------------------
-- Records of kt_gptcms_content_security
-- ----------------------------

-- ----------------------------
-- Table structure for `kt_gptcms_create_msg`
-- ----------------------------
DROP TABLE IF EXISTS `kt_gptcms_create_msg`;
CREATE TABLE `kt_gptcms_create_msg` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `wid` int(11) DEFAULT NULL COMMENT '用户id',
  `common_id` int(11) DEFAULT NULL COMMENT '终端用户id',
  `model_id` int(11) DEFAULT NULL COMMENT '创作模型id',
  `un_message` text CHARACTER SET utf8mb4 COMMENT '用户发出内容(未过滤)',
  `message` text CHARACTER SET utf8mb4 COMMENT '用户发出内容(已过滤)',
  `make_message` text CHARACTER SET utf8mb4 COMMENT '最终组合提交接口的内容',
  `un_response` text CHARACTER SET utf8mb4 COMMENT '助手回复内容(未过滤)',
  `response` text CHARACTER SET utf8mb4 COMMENT '助手回复内容(已过滤)',
  `total_tokens` int(11) DEFAULT NULL COMMENT '发出+回复字符串的总长度',
  `c_time` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=62 DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;

-- ----------------------------
-- Records of kt_gptcms_create_msg
-- ----------------------------

-- ----------------------------
-- Table structure for `kt_gptcms_draw_classify`
-- ----------------------------
DROP TABLE IF EXISTS `kt_gptcms_draw_classify`;
CREATE TABLE `kt_gptcms_draw_classify` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `wid` int(11) DEFAULT NULL,
  `title` varchar(255) DEFAULT NULL COMMENT '名称',
  `sort` int(11) DEFAULT '0' COMMENT '排序',
  `c_time` datetime DEFAULT NULL,
  `u_time` datetime DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC COMMENT='作品分类';

-- ----------------------------
-- Records of kt_gptcms_draw_classify
-- ----------------------------

-- ----------------------------
-- Table structure for `kt_gptcms_draw_desclexicon`
-- ----------------------------
DROP TABLE IF EXISTS `kt_gptcms_draw_desclexicon`;
CREATE TABLE `kt_gptcms_draw_desclexicon` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `wid` int(11) DEFAULT NULL,
  `content` varchar(255) DEFAULT NULL COMMENT '词库内容',
  `xh` int(11) DEFAULT NULL COMMENT '序号 越大越靠前',
  `c_time` datetime DEFAULT NULL,
  `u_time` datetime DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC COMMENT='高级绘画 描述词库';

-- ----------------------------
-- Records of kt_gptcms_draw_desclexicon
-- ----------------------------

-- ----------------------------
-- Table structure for `kt_gptcms_draw_msg`
-- ----------------------------
DROP TABLE IF EXISTS `kt_gptcms_draw_msg`;
CREATE TABLE `kt_gptcms_draw_msg` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `wid` int(11) DEFAULT NULL COMMENT '用户id',
  `common_id` int(11) DEFAULT NULL COMMENT '终端用户id',
  `un_message` text CHARACTER SET utf16 COMMENT '用户发出内容(未过滤)',
  `message` text COMMENT '用户发出内容(已过滤)',
  `un_response` text COMMENT '助手回复内容(未过滤)',
  `response` text COMMENT '助手回复内容(已过滤)',
  `total_tokens` int(11) DEFAULT NULL COMMENT '发出+回复字符串的总长度',
  `c_time` int(11) DEFAULT NULL,
  `status` tinyint(1) DEFAULT '1' COMMENT '0生成失败 1处理中 2处理成功',
  `u_time` int(11) DEFAULT NULL,
  `sync_status` tinyint(1) DEFAULT '0' COMMENT '1已同步 0未同步',
  `chatmodel` varchar(255) DEFAULT NULL COMMENT '渠道',
  `style` varchar(255) DEFAULT NULL COMMENT '风格指令',
  `size` varchar(20) DEFAULT NULL COMMENT '尺寸',
  `pid` int(11) DEFAULT '0' COMMENT '放大上级id',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;

-- ----------------------------
-- Records of kt_gptcms_draw_msg
-- ----------------------------

-- ----------------------------
-- Table structure for `kt_gptcms_draw_msgtp`
-- ----------------------------
DROP TABLE IF EXISTS `kt_gptcms_draw_msgtp`;
CREATE TABLE `kt_gptcms_draw_msgtp` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `wid` int(11) DEFAULT NULL,
  `common_id` int(11) DEFAULT NULL COMMENT '用户id',
  `msg_id` int(11) DEFAULT NULL COMMENT '消息id',
  `message` varchar(255) DEFAULT NULL COMMENT '提示词',
  `classfy_id` int(11) DEFAULT NULL COMMENT '分类id',
  `title` varchar(255) DEFAULT NULL COMMENT '标题',
  `image` text COMMENT '图片地址',
  `del_status` tinyint(1) DEFAULT '0' COMMENT '是否删除 1删除  0未删除 ',
  `open_status` tinyint(1) DEFAULT '0' COMMENT '公开状态 1公开 0私密',
  `hot_status` tinyint(1) DEFAULT '0' COMMENT '热门状态 1热门 0不是热门',
  `c_time` datetime DEFAULT NULL,
  `u_time` datetime DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;

-- ----------------------------
-- Records of kt_gptcms_draw_msgtp
-- ----------------------------

-- ----------------------------
-- Table structure for `kt_gptcms_draw_notify`
-- ----------------------------
DROP TABLE IF EXISTS `kt_gptcms_draw_notify`;
CREATE TABLE `kt_gptcms_draw_notify` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `wid` int(11) DEFAULT NULL,
  `task_id` varchar(255) DEFAULT NULL COMMENT '回调任务di',
  `chatmodel` varchar(255) DEFAULT NULL COMMENT '渠道',
  `msgid` int(11) DEFAULT NULL COMMENT '消息iid',
  `c_time` datetime DEFAULT NULL,
  `status` tinyint(1) DEFAULT '0' COMMENT '0未回调  1已回调',
  `u_time` datetime DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;

-- ----------------------------
-- Records of kt_gptcms_draw_notify
-- ----------------------------

-- ----------------------------
-- Table structure for `kt_gptcms_draw_style`
-- ----------------------------
DROP TABLE IF EXISTS `kt_gptcms_draw_style`;
CREATE TABLE `kt_gptcms_draw_style` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `wid` int(11) DEFAULT NULL,
  `title` varchar(255) DEFAULT NULL COMMENT '名称',
  `tp_url` varchar(255) DEFAULT NULL COMMENT '图片url',
  `desc` varchar(255) DEFAULT NULL COMMENT '指令',
  `xh` int(11) DEFAULT '0' COMMENT '序号 越大越靠前',
  `vip_status` tinyint(1) DEFAULT '0' COMMENT 'vip是否可使用 1可以使用 0不能使用',
  `status` tinyint(1) DEFAULT '1' COMMENT '状态 1正常 0暂停使用',
  `c_time` datetime DEFAULT NULL,
  `u_time` datetime DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC COMMENT='风格';

-- ----------------------------
-- Records of kt_gptcms_draw_style
-- ----------------------------

-- ----------------------------
-- Table structure for `kt_gptcms_gpt_config`
-- ----------------------------
DROP TABLE IF EXISTS `kt_gptcms_gpt_config`;
CREATE TABLE `kt_gptcms_gpt_config` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `wid` int(11) DEFAULT NULL,
  `channel` tinyint(1) DEFAULT '1' COMMENT '渠道1.openai 2.api2d 3.文心一言 4.通义千问 5.昆仑天工 6.ChatGLM',
  `openai` text COMMENT 'openai',
  `api2d` text COMMENT 'api2d',
  `wxyy` text COMMENT '文心一言配置',
  `tyqw` text COMMENT '通义千文',
  `kltg` text COMMENT '昆仑天工',
  `chatglm` text COMMENT 'chatglm',
  `linkerai` text COMMENT '灵犀星火',
  `gpt4` text,
  `api2d4` text,
  `u_time` datetime DEFAULT NULL,
  `c_time` datetime DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;

-- ----------------------------
-- Records of kt_gptcms_gpt_config
-- ----------------------------

-- ----------------------------
-- Table structure for `kt_gptcms_gptpaint_config`
-- ----------------------------
DROP TABLE IF EXISTS `kt_gptcms_gptpaint_config`;
CREATE TABLE `kt_gptcms_gptpaint_config` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `wid` int(11) DEFAULT NULL,
  `channel` tinyint(1) DEFAULT '1' COMMENT '渠道1 意间',
  `yjai` text COMMENT '意间AI',
  `u_time` datetime DEFAULT NULL,
  `c_time` datetime DEFAULT NULL,
  `replicate` text,
  `status` tinyint(1) DEFAULT '1' COMMENT '是否开启绘画 1开启  0关闭',
  `draw_status` tinyint(1) DEFAULT '0' COMMENT '是否开启高级绘画 1开启 0关闭',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;

-- ----------------------------
-- Records of kt_gptcms_gptpaint_config
-- ----------------------------

-- ----------------------------
-- Table structure for `kt_gptcms_gzh_interest`
-- ----------------------------
DROP TABLE IF EXISTS `kt_gptcms_gzh_interest`;
CREATE TABLE `kt_gptcms_gzh_interest` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `wid` int(10) DEFAULT NULL,
  `content` text COMMENT '回复消息列表',
  `ctime` datetime DEFAULT NULL,
  `status` int(10) DEFAULT '1' COMMENT '1为启动，2为关闭，默认为关闭',
  `type` int(10) DEFAULT NULL COMMENT '1为被关注回复，2为收到消息回复',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;

-- ----------------------------
-- Records of kt_gptcms_gzh_interest
-- ----------------------------

-- ----------------------------
-- Table structure for `kt_gptcms_gzh_keyword`
-- ----------------------------
DROP TABLE IF EXISTS `kt_gptcms_gzh_keyword`;
CREATE TABLE `kt_gptcms_gzh_keyword` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `wid` int(10) DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL COMMENT '规则名称',
  `type` varchar(255) DEFAULT '1' COMMENT '规则类型，1为全匹配，2为半匹配，默认为2',
  `word` varchar(255) DEFAULT NULL COMMENT '关键词',
  `content` text COMMENT '回复内容',
  `reply_type` int(10) DEFAULT '1' COMMENT '回复方式，默认为随机回复一条，1为随机，2为全部回复',
  `ctime` datetime DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;

-- ----------------------------
-- Records of kt_gptcms_gzh_keyword
-- ----------------------------

-- ----------------------------
-- Table structure for `kt_gptcms_h5_wx`
-- ----------------------------
DROP TABLE IF EXISTS `kt_gptcms_h5_wx`;
CREATE TABLE `kt_gptcms_h5_wx` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `wid` int(11) DEFAULT NULL,
  `title` varchar(255) DEFAULT NULL COMMENT '页面标题',
  `share_tile` varchar(255) DEFAULT NULL COMMENT '分享标题',
  `share_desc` varchar(255) DEFAULT NULL COMMENT '分享描述',
  `share_image` varchar(255) DEFAULT NULL COMMENT '分享图片链接',
  `status` tinyint(1) DEFAULT '0' COMMENT '微信登陆 1开启 0关闭',
  `create_time` datetime DEFAULT NULL,
  `update_time` datetime DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;

-- ----------------------------
-- Records of kt_gptcms_h5_wx
-- ----------------------------

-- ----------------------------
-- Table structure for `kt_gptcms_hot`
-- ----------------------------
DROP TABLE IF EXISTS `kt_gptcms_hot`;
CREATE TABLE `kt_gptcms_hot` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `wid` int(11) DEFAULT NULL,
  `content` text COMMENT '内容',
  `sort` int(11) DEFAULT '0' COMMENT '越大越靠前',
  `classify_id` int(11) DEFAULT NULL COMMENT '分类id',
  `c_time` datetime DEFAULT NULL,
  `u_time` datetime DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;

-- ----------------------------
-- Records of kt_gptcms_hot
-- ----------------------------

-- ----------------------------
-- Table structure for `kt_gptcms_hot_classify`
-- ----------------------------
DROP TABLE IF EXISTS `kt_gptcms_hot_classify`;
CREATE TABLE `kt_gptcms_hot_classify` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `wid` int(11) DEFAULT NULL,
  `title` varchar(255) DEFAULT NULL COMMENT '名称',
  `icon` varchar(255) DEFAULT NULL COMMENT '图标',
  `sort` int(11) DEFAULT '0' COMMENT '排序',
  `c_time` datetime DEFAULT NULL,
  `u_time` datetime DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC COMMENT='热门分类';

-- ----------------------------
-- Records of kt_gptcms_hot_classify
-- ----------------------------

-- ----------------------------
-- Table structure for `kt_gptcms_invite_award`
-- ----------------------------
DROP TABLE IF EXISTS `kt_gptcms_invite_award`;
CREATE TABLE `kt_gptcms_invite_award` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `wid` int(11) DEFAULT NULL,
  `status` tinyint(1) DEFAULT NULL COMMENT ' 1开启 0关闭',
  `number` int(11) DEFAULT NULL COMMENT '邀请一次奖励数量',
  `up_limit` int(11) DEFAULT NULL COMMENT '邀请人数上限',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC COMMENT='邀请奖励';

-- ----------------------------
-- Records of kt_gptcms_invite_award
-- ----------------------------

-- ----------------------------
-- Table structure for `kt_gptcms_jmodel`
-- ----------------------------
DROP TABLE IF EXISTS `kt_gptcms_jmodel`;
CREATE TABLE `kt_gptcms_jmodel` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `wid` int(11) DEFAULT NULL,
  `title` varchar(255) DEFAULT NULL COMMENT '名称',
  `tp_url` varchar(255) DEFAULT NULL COMMENT '图片url',
  `desc` varchar(255) DEFAULT NULL COMMENT '描述',
  `bz` varchar(255) DEFAULT NULL COMMENT '备注',
  `xh` int(11) DEFAULT '0' COMMENT '序号 越大越靠前',
  `vip_status` tinyint(1) DEFAULT '0' COMMENT 'vip是否可使用 1可以使用 0不能使用',
  `status` tinyint(1) DEFAULT '1' COMMENT '状态 1正常 0暂停使用',
  `c_time` datetime DEFAULT NULL,
  `u_time` datetime DEFAULT NULL,
  `classify_id` int(5) DEFAULT NULL COMMENT '分类id',
  `content` text COMMENT '模型内容',
  `hint_content` text COMMENT '对话框提示文字',
  `defalut_question` text COMMENT '默认问题',
  `defalut_reply` text COMMENT '默认回复',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=35 DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC COMMENT='角色模型';

-- ----------------------------
-- Records of kt_gptcms_jmodel
-- ----------------------------

-- ----------------------------
-- Table structure for `kt_gptcms_jmodel_classify`
-- ----------------------------
DROP TABLE IF EXISTS `kt_gptcms_jmodel_classify`;
CREATE TABLE `kt_gptcms_jmodel_classify` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `wid` int(11) DEFAULT NULL,
  `title` varchar(255) DEFAULT NULL,
  `sort` int(11) DEFAULT '0' COMMENT '排序',
  `c_time` datetime DEFAULT NULL,
  `u_time` datetime DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC COMMENT='角色模型分类';

-- ----------------------------
-- Records of kt_gptcms_jmodel_classify
-- ----------------------------

-- ----------------------------
-- Table structure for `kt_gptcms_keys`
-- ----------------------------
DROP TABLE IF EXISTS `kt_gptcms_keys`;
CREATE TABLE `kt_gptcms_keys` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `wid` int(11) DEFAULT NULL,
  `chatmodel` varchar(255) DEFAULT NULL COMMENT '渠道',
  `key` varchar(255) DEFAULT NULL COMMENT 'key',
  `state` tinyint(1) DEFAULT '1' COMMENT '状态0停用1正常',
  `stop_reason` varchar(255) DEFAULT NULL COMMENT '停用原因',
  `ctime` int(11) DEFAULT NULL,
  `utime` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;

-- ----------------------------
-- Records of kt_gptcms_keys
-- ----------------------------

-- ----------------------------
-- Table structure for `kt_gptcms_keys_switch`
-- ----------------------------
DROP TABLE IF EXISTS `kt_gptcms_keys_switch`;
CREATE TABLE `kt_gptcms_keys_switch` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `wid` int(11) DEFAULT NULL,
  `chatmodel` varchar(255) DEFAULT NULL COMMENT '渠道',
  `switch` tinyint(1) DEFAULT '0' COMMENT '开关0关闭1开启',
  `utime` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;

-- ----------------------------
-- Records of kt_gptcms_keys_switch
-- ----------------------------

-- ----------------------------
-- Table structure for `kt_gptcms_menu`
-- ----------------------------
DROP TABLE IF EXISTS `kt_gptcms_menu`;
CREATE TABLE `kt_gptcms_menu` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `wid` int(10) NOT NULL,
  `pid` int(10) DEFAULT '0' COMMENT '本表中一级菜单id',
  `type` int(10) DEFAULT NULL COMMENT '1为一级菜单，2为二级菜单',
  `menu_type` int(10) DEFAULT NULL COMMENT '菜单类型，1为关键词，2为小程序，3为跳转url',
  `keys` varchar(255) DEFAULT NULL COMMENT '菜单类型为1，key值',
  `ctime` datetime DEFAULT NULL,
  `appid` varchar(255) DEFAULT NULL COMMENT '菜单类型为2，小程序appid',
  `url` varchar(255) DEFAULT NULL COMMENT '菜单类型为2，小程序url',
  `pagepath` varchar(255) DEFAULT NULL COMMENT '菜单类型为2，小程序的页面路径',
  `menu_url` varchar(255) DEFAULT NULL COMMENT '菜单类型为3，url',
  `order` int(10) DEFAULT NULL COMMENT '排序，1-5,1-3',
  `name` varchar(255) DEFAULT NULL COMMENT '名称',
  `status` int(10) DEFAULT '1' COMMENT '1为正常，2为删除',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;

-- ----------------------------
-- Records of kt_gptcms_menu
-- ----------------------------

-- ----------------------------
-- Table structure for `kt_gptcms_miniprogram`
-- ----------------------------
DROP TABLE IF EXISTS `kt_gptcms_miniprogram`;
CREATE TABLE `kt_gptcms_miniprogram` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `wid` int(11) DEFAULT NULL,
  `appid` varchar(255) DEFAULT NULL,
  `appsecret` varchar(255) DEFAULT NULL,
  `mch_id` varchar(255) DEFAULT NULL,
  `mch_key` varchar(255) DEFAULT NULL,
  `c_time` datetime DEFAULT NULL,
  `u_time` datetime DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;

-- ----------------------------
-- Records of kt_gptcms_miniprogram
-- ----------------------------

-- ----------------------------
-- Table structure for `kt_gptcms_paint_msg`
-- ----------------------------
DROP TABLE IF EXISTS `kt_gptcms_paint_msg`;
CREATE TABLE `kt_gptcms_paint_msg` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `wid` int(11) DEFAULT NULL COMMENT '用户id',
  `common_id` int(11) DEFAULT NULL COMMENT '终端用户id',
  `un_message` text CHARACTER SET utf16 COMMENT '用户发出内容(未过滤)',
  `message` text COMMENT '用户发出内容(已过滤)',
  `un_response` text COMMENT '助手回复内容(未过滤)',
  `response` text COMMENT '助手回复内容(已过滤)',
  `total_tokens` int(11) DEFAULT NULL COMMENT '发出 回复字符串的总长度',
  `c_time` int(11) DEFAULT NULL,
  `status` tinyint(1) DEFAULT '1' COMMENT '0生成失败 1处理中 2处理成功',
  `u_time` int(11) DEFAULT NULL,
  `sync_status` tinyint(1) DEFAULT '0' COMMENT '1已同步 0未同步',
  `chatmodel` varchar(255) DEFAULT NULL COMMENT '渠道',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;

-- ----------------------------
-- Records of kt_gptcms_paint_msg
-- ----------------------------

-- ----------------------------
-- Table structure for `kt_gptcms_paintmodel_set`
-- ----------------------------
DROP TABLE IF EXISTS `kt_gptcms_paintmodel_set`;
CREATE TABLE `kt_gptcms_paintmodel_set` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `wid` int(11) DEFAULT NULL,
  `sd` text COMMENT '灵犀星火 sd绘画',
  `yjai` text COMMENT '意间Ai',
  `gpt35` text COMMENT 'openai 绘画',
  `api2d35` text COMMENT 'api2d绘画',
  `replicate` text COMMENT 'replicateMJ',
  `linkerai_mj` text COMMENT '灵犀星火 MJ绘画',
  `status` tinyint(1) DEFAULT '0' COMMENT '1开启 0关闭',
  `c_time` datetime DEFAULT NULL,
  `u_time` datetime DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC COMMENT='绘画模型设置';

-- ----------------------------
-- Records of kt_gptcms_paintmodel_set
-- ----------------------------

-- ----------------------------
-- Table structure for `kt_gptcms_paintmsg_notify`
-- ----------------------------
DROP TABLE IF EXISTS `kt_gptcms_paintmsg_notify`;
CREATE TABLE `kt_gptcms_paintmsg_notify` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `wid` int(11) DEFAULT NULL,
  `task_id` varchar(255) DEFAULT NULL COMMENT '回调任务di',
  `chatmodel` varchar(255) DEFAULT NULL COMMENT '渠道',
  `msgid` int(11) DEFAULT NULL COMMENT '消息id',
  `c_time` datetime DEFAULT NULL,
  `status` tinyint(1) DEFAULT '0' COMMENT '0未回调  1已回调',
  `u_time` datetime DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;

-- ----------------------------
-- Records of kt_gptcms_paintmsg_notify
-- ----------------------------

-- ----------------------------
-- Table structure for `kt_gptcms_pay`
-- ----------------------------
DROP TABLE IF EXISTS `kt_gptcms_pay`;
CREATE TABLE `kt_gptcms_pay` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `out_trade_no` char(50) DEFAULT NULL COMMENT '微信自定义订单号',
  `orderid` int(11) DEFAULT NULL COMMENT '订单id',
  `order_bh` char(50) DEFAULT NULL COMMENT '订单编号',
  `wid` int(11) DEFAULT NULL COMMENT '账户id',
  `common_id` int(11) DEFAULT NULL COMMENT '用户id',
  `uip` char(50) DEFAULT NULL COMMENT '请求ip',
  `amount` decimal(10,2) DEFAULT NULL COMMENT '金额',
  `status` char(50) DEFAULT NULL COMMENT '状态',
  `alipayzt` char(50) DEFAULT NULL,
  `bz` varchar(250) DEFAULT NULL,
  `ifok` int(11) DEFAULT NULL COMMENT '是否支付 1:已支付  0:未支付',
  `wxddbh` char(50) DEFAULT NULL COMMENT '微信订单编号',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `update_time` datetime DEFAULT NULL,
  `jyh` varchar(255) DEFAULT NULL COMMENT '交易单号',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=57 DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;

-- ----------------------------
-- Records of kt_gptcms_pay
-- ----------------------------

-- ----------------------------
-- Table structure for `kt_gptcms_pay_config`
-- ----------------------------
DROP TABLE IF EXISTS `kt_gptcms_pay_config`;
CREATE TABLE `kt_gptcms_pay_config` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `wid` int(11) DEFAULT NULL,
  `config` text COMMENT '支付配置',
  `type` varchar(100) DEFAULT NULL COMMENT '类型: wx 微信  ali 支付宝',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC COMMENT='支付配置';

-- ----------------------------
-- Records of kt_gptcms_pay_config
-- ----------------------------

-- ----------------------------
-- Table structure for `kt_gptcms_pay_order`
-- ----------------------------
DROP TABLE IF EXISTS `kt_gptcms_pay_order`;
CREATE TABLE `kt_gptcms_pay_order` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `wid` int(11) DEFAULT NULL,
  `common_id` int(11) DEFAULT NULL,
  `pay_time` datetime DEFAULT NULL COMMENT '支付时间',
  `c_time` datetime DEFAULT NULL,
  `u_time` datetime DEFAULT NULL,
  `amount` decimal(10,2) DEFAULT NULL COMMENT '金额',
  `order_bh` varchar(255) DEFAULT NULL COMMENT '订单编号',
  `status` tinyint(1) DEFAULT '1' COMMENT '1待支付 2支付成功 3取消支付',
  `number` int(11) DEFAULT NULL COMMENT '数量',
  `type` tinyint(1) DEFAULT '9' COMMENT '1天 2周 3月 4季度  5年 9条',
  `pay_type` varchar(255) DEFAULT NULL COMMENT 'wx 微信   alipay支付宝',
  `ddbh` varchar(255) DEFAULT NULL COMMENT '微信订单号',
  `setmeal_type` varchar(255) DEFAULT NULL COMMENT 'vip VIP套餐  recharge充值套餐',
  `setmeal_id` int(11) DEFAULT NULL COMMENT '套餐表id',
  `buy_number` int(11) DEFAULT NULL COMMENT '购买数量',
  `transaction_id` varchar(255) DEFAULT NULL COMMENT '微信交易订单号',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=81 DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC COMMENT='支付订单记录';

-- ----------------------------
-- Records of kt_gptcms_pay_order
-- ----------------------------

-- ----------------------------
-- Table structure for `kt_gptcms_pc`
-- ----------------------------
DROP TABLE IF EXISTS `kt_gptcms_pc`;
CREATE TABLE `kt_gptcms_pc` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `wid` int(11) DEFAULT NULL,
  `title` varchar(255) DEFAULT NULL COMMENT '页面标题',
  `bottom_desc` varchar(255) DEFAULT NULL COMMENT '底部声明',
  `desc_link` varchar(255) DEFAULT NULL COMMENT '生命链接',
  `status` tinyint(1) DEFAULT '0' COMMENT '微信登陆 1开启 0关闭',
  `create_time` datetime DEFAULT NULL,
  `update_time` datetime DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;

-- ----------------------------
-- Records of kt_gptcms_pc
-- ----------------------------

-- ----------------------------
-- Table structure for `kt_gptcms_qzzl`
-- ----------------------------
DROP TABLE IF EXISTS `kt_gptcms_qzzl`;
CREATE TABLE `kt_gptcms_qzzl` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `wid` int(11) DEFAULT NULL,
  `status` tinyint(1) DEFAULT '0',
  `content` text,
  `u_time` datetime DEFAULT NULL,
  `c_time` datetime DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;

-- ----------------------------
-- Records of kt_gptcms_qzzl
-- ----------------------------

-- ----------------------------
-- Table structure for `kt_gptcms_random`
-- ----------------------------
DROP TABLE IF EXISTS `kt_gptcms_random`;
CREATE TABLE `kt_gptcms_random` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `openid` varchar(255) DEFAULT NULL COMMENT '访问的客户openid',
  `random` varchar(255) DEFAULT NULL COMMENT '生成所携带的随机数',
  `ctime` datetime DEFAULT NULL,
  `wid` int(10) DEFAULT NULL,
  `code` varchar(255) DEFAULT NULL COMMENT '二维码图片',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;

-- ----------------------------
-- Records of kt_gptcms_random
-- ----------------------------

-- ----------------------------
-- Table structure for `kt_gptcms_recharge_setmeal`
-- ----------------------------
DROP TABLE IF EXISTS `kt_gptcms_recharge_setmeal`;
CREATE TABLE `kt_gptcms_recharge_setmeal` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `wid` int(11) DEFAULT NULL,
  `title` varchar(255) DEFAULT NULL,
  `sequence` int(11) DEFAULT NULL COMMENT '序号  越大越靠前',
  `number` int(11) DEFAULT NULL COMMENT '数量',
  `price` decimal(10,2) DEFAULT NULL COMMENT '价格',
  `old_price` decimal(10,2) DEFAULT NULL COMMENT '划线价',
  `bz` varchar(255) DEFAULT NULL,
  `c_time` datetime DEFAULT NULL,
  `u_time` datetime DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;

-- ----------------------------
-- Records of kt_gptcms_recharge_setmeal
-- ----------------------------

-- ----------------------------
-- Table structure for `kt_gptcms_reward_record`
-- ----------------------------
DROP TABLE IF EXISTS `kt_gptcms_reward_record`;
CREATE TABLE `kt_gptcms_reward_record` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `wid` int(11) DEFAULT NULL COMMENT '用户id',
  `common_id` int(11) DEFAULT NULL COMMENT '终端用户id',
  `num` int(11) DEFAULT NULL COMMENT '奖励条数',
  `type` tinyint(1) DEFAULT '1' COMMENT '奖励类型 1注册 2登录 3邀请',
  `c_time` datetime DEFAULT NULL COMMENT '奖励时间',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;

-- ----------------------------
-- Records of kt_gptcms_reward_record
-- ----------------------------

-- ----------------------------
-- Table structure for `kt_gptcms_role_msg`
-- ----------------------------
DROP TABLE IF EXISTS `kt_gptcms_role_msg`;
CREATE TABLE `kt_gptcms_role_msg` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `wid` int(11) DEFAULT NULL COMMENT '用户id',
  `common_id` int(11) DEFAULT NULL COMMENT '终端用户id',
  `model_id` int(11) DEFAULT NULL COMMENT '角色模型id',
  `tip_message` text CHARACTER SET utf8mb4 COMMENT '指令内容',
  `un_message` text CHARACTER SET utf8mb4 COMMENT '用户发出内容(未过滤)',
  `message` text CHARACTER SET utf8mb4 COMMENT '用户发出内容(已过滤)',
  `un_response` text CHARACTER SET utf8mb4 COMMENT '助手回复内容(未过滤)',
  `response` text CHARACTER SET utf8mb4 COMMENT '助手回复内容(已过滤)',
  `total_tokens` int(11) DEFAULT NULL COMMENT '发出+回复字符串的总长度',
  `c_time` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=29 DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;

-- ----------------------------
-- Records of kt_gptcms_role_msg
-- ----------------------------

-- ----------------------------
-- Table structure for `kt_gptcms_share_award`
-- ----------------------------
DROP TABLE IF EXISTS `kt_gptcms_share_award`;
CREATE TABLE `kt_gptcms_share_award` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `wid` int(11) DEFAULT NULL,
  `status` tinyint(1) DEFAULT NULL COMMENT ' 1开启 0关闭',
  `number` int(11) DEFAULT NULL COMMENT '分享一次奖励数量',
  `up_limit` int(11) DEFAULT NULL COMMENT '分享数量上限',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC COMMENT='分享奖励';

-- ----------------------------
-- Records of kt_gptcms_share_award
-- ----------------------------

-- ----------------------------
-- Table structure for `kt_gptcms_share_rewards`
-- ----------------------------
DROP TABLE IF EXISTS `kt_gptcms_share_rewards`;
CREATE TABLE `kt_gptcms_share_rewards` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `wid` int(11) DEFAULT NULL COMMENT '用户id',
  `common_id` int(11) DEFAULT NULL COMMENT '终端用户id',
  `num` int(11) DEFAULT NULL COMMENT '奖励条数',
  `c_time` datetime DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;

-- ----------------------------
-- Records of kt_gptcms_share_rewards
-- ----------------------------

-- ----------------------------
-- Table structure for `kt_gptcms_sms_config`
-- ----------------------------
DROP TABLE IF EXISTS `kt_gptcms_sms_config`;
CREATE TABLE `kt_gptcms_sms_config` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `wid` int(11) DEFAULT NULL COMMENT '账户id',
  `access_key_id` varchar(255) DEFAULT NULL COMMENT '阿里云短信key',
  `access_key_secret` varchar(255) DEFAULT NULL COMMENT '阿里云短信密钥',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC COMMENT='阿里云短信配置';

-- ----------------------------
-- Records of kt_gptcms_sms_config
-- ----------------------------

-- ----------------------------
-- Table structure for `kt_gptcms_sms_template`
-- ----------------------------
DROP TABLE IF EXISTS `kt_gptcms_sms_template`;
CREATE TABLE `kt_gptcms_sms_template` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `wid` int(11) DEFAULT NULL,
  `bh` varchar(20) DEFAULT NULL,
  `sign_name` varchar(100) DEFAULT NULL,
  `template_code` varchar(100) DEFAULT NULL,
  `content` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;

-- ----------------------------
-- Records of kt_gptcms_sms_template
-- ----------------------------

-- ----------------------------
-- Table structure for `kt_gptcms_storage_config`
-- ----------------------------
DROP TABLE IF EXISTS `kt_gptcms_storage_config`;
CREATE TABLE `kt_gptcms_storage_config` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `wid` int(11) DEFAULT NULL COMMENT '账户id',
  `type` tinyint(1) DEFAULT '2' COMMENT '2:oss阿里云 3: cos 腾讯云 4: kodo七牛云',
  `oss_id` varchar(100) DEFAULT NULL COMMENT '阿里云 id',
  `oss_secret` varchar(100) DEFAULT NULL COMMENT '阿里云 secret',
  `oss_endpoint` varchar(100) DEFAULT NULL COMMENT '阿里云  访问域名',
  `oss_bucket` varchar(100) DEFAULT NULL COMMENT '阿里云bucket',
  `cos_secretId` varchar(100) DEFAULT NULL COMMENT '腾讯云 id',
  `cos_secretKey` varchar(100) DEFAULT NULL COMMENT '腾讯云 key',
  `cos_bucket` varchar(100) DEFAULT NULL COMMENT '腾讯云 bucket',
  `cos_endpoint` varchar(100) DEFAULT NULL COMMENT '腾讯云 endpoint',
  `kodo_key` varchar(100) DEFAULT NULL COMMENT '七牛云 key',
  `kodo_secret` varchar(100) DEFAULT NULL COMMENT '七牛云 secret',
  `kodo_domain` varchar(100) DEFAULT NULL COMMENT '七牛云 domain',
  `kodo_bucket` varchar(100) DEFAULT NULL COMMENT '七牛云 bucket',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `update_time` datetime DEFAULT NULL COMMENT '更新时间',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC COMMENT='云存储配置';

-- ----------------------------
-- Records of kt_gptcms_storage_config
-- ----------------------------

-- ----------------------------
-- Table structure for `kt_gptcms_system`
-- ----------------------------
DROP TABLE IF EXISTS `kt_gptcms_system`;
CREATE TABLE `kt_gptcms_system` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `wid` int(11) DEFAULT NULL,
  `rz_number` int(11) DEFAULT '0' COMMENT '注册赠送次数',
  `dz_number` int(11) DEFAULT '0' COMMENT '每日赠送次数',
  `dz_remind` text COMMENT '超出每日赠送次数提醒',
  `zdz_number` int(11) DEFAULT NULL COMMENT '总每日赠送次数限制',
  `zdz_remind` varchar(255) DEFAULT NULL COMMENT '超出每日总次数提示语',
  `yq_number` int(11) DEFAULT '0' COMMENT '邀请奖励次数',
  `welcome` text COMMENT '欢迎语',
  `sms` tinyint(1) DEFAULT '0' COMMENT '短信开关 1开启 0关闭',
  `self_balance` varchar(20) DEFAULT NULL COMMENT '自定义余额',
  `gpt4_charging` tinyint(1) DEFAULT '0' COMMENT 'GPT4单独计费 1开启 0关闭 ',
  `lxmj_charging` tinyint(1) DEFAULT '0' COMMENT '灵犀-MJ单独计费 1开启 0关闭 ',
  `create_time` datetime DEFAULT NULL,
  `update_time` datetime DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;

-- ----------------------------
-- Records of kt_gptcms_system
-- ----------------------------

-- ----------------------------
-- Table structure for `kt_gptcms_tencentai_config`
-- ----------------------------
DROP TABLE IF EXISTS `kt_gptcms_tencentai_config`;
CREATE TABLE `kt_gptcms_tencentai_config` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `wid` int(11) DEFAULT NULL,
  `secret_id` varchar(255) DEFAULT NULL,
  `secret_key` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;

-- ----------------------------
-- Records of kt_gptcms_tencentai_config
-- ----------------------------

-- ----------------------------
-- Table structure for `kt_gptcms_vad_award`
-- ----------------------------
DROP TABLE IF EXISTS `kt_gptcms_vad_award`;
CREATE TABLE `kt_gptcms_vad_award` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `wid` int(11) DEFAULT NULL,
  `status` tinyint(1) DEFAULT NULL COMMENT ' 1开启 0关闭',
  `number` int(11) DEFAULT NULL COMMENT '邀请一次奖励数量',
  `up_limit` int(11) DEFAULT NULL COMMENT '邀请人数上限',
  `ad_id` varchar(255) DEFAULT NULL COMMENT '广告位id',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC COMMENT='观看广告奖励';

-- ----------------------------
-- Records of kt_gptcms_vad_award
-- ----------------------------

-- ----------------------------
-- Table structure for `kt_gptcms_vip_equity`
-- ----------------------------
DROP TABLE IF EXISTS `kt_gptcms_vip_equity`;
CREATE TABLE `kt_gptcms_vip_equity` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `wid` int(11) DEFAULT NULL,
  `explain` varchar(2000) DEFAULT NULL COMMENT '权益说明',
  `utime` datetime DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;

-- ----------------------------
-- Records of kt_gptcms_vip_equity
-- ----------------------------

-- ----------------------------
-- Table structure for `kt_gptcms_vip_setmeal`
-- ----------------------------
DROP TABLE IF EXISTS `kt_gptcms_vip_setmeal`;
CREATE TABLE `kt_gptcms_vip_setmeal` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `wid` int(11) DEFAULT NULL,
  `title` varchar(255) DEFAULT NULL,
  `sort` int(11) DEFAULT '0' COMMENT '序号 越大越靠前',
  `duration` int(11) DEFAULT NULL COMMENT '时长',
  `duration_type` tinyint(1) DEFAULT NULL COMMENT '1天 2周 3月 4季度  5年',
  `price` decimal(10,2) DEFAULT NULL COMMENT '价格',
  `old_price` decimal(10,2) DEFAULT NULL COMMENT '原价',
  `give_num` int(11) DEFAULT '0' COMMENT '开通VIP会员一次性赠送条数',
  `c_time` datetime DEFAULT NULL,
  `u_time` datetime DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;

-- ----------------------------
-- Records of kt_gptcms_vip_setmeal
-- ----------------------------

-- ----------------------------
-- Table structure for `kt_gptcms_websit`
-- ----------------------------
DROP TABLE IF EXISTS `kt_gptcms_websit`;
CREATE TABLE `kt_gptcms_websit` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `wid` int(11) DEFAULT NULL,
  `title` varchar(255) DEFAULT NULL COMMENT '站点标题',
  `sms` tinyint(1) DEFAULT '0',
  `kfcode` varchar(255) DEFAULT NULL COMMENT '客服二维码',
  `gzhcode` varchar(255) DEFAULT NULL COMMENT '公众号二维码',
  `pcwxlogin_status` tinyint(1) DEFAULT '0' COMMENT 'pc微信登录开关  1开启 0关闭',
  `create_time` datetime DEFAULT NULL,
  `update_time` datetime DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;

-- ----------------------------
-- Records of kt_gptcms_websit
-- ----------------------------

-- ----------------------------
-- Table structure for `kt_gptcms_wx_user`
-- ----------------------------
DROP TABLE IF EXISTS `kt_gptcms_wx_user`;
CREATE TABLE `kt_gptcms_wx_user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `wid` int(11) DEFAULT NULL COMMENT '系统用户id',
  `common_id` int(11) DEFAULT NULL COMMENT '关联基础用户id',
  `openid` varchar(255) DEFAULT NULL COMMENT 'openid',
  `mobile` varchar(255) DEFAULT NULL COMMENT '手机号',
  `nickname` varchar(255) DEFAULT NULL COMMENT '昵称',
  `headimgurl` varchar(255) DEFAULT NULL COMMENT '头像',
  `sex` tinyint(1) DEFAULT '0' COMMENT '用户的性别，值为1时是男性，值为2时是女性，值为0时是未知',
  `city` varchar(255) DEFAULT NULL COMMENT '市',
  `province` varchar(255) DEFAULT NULL COMMENT '省',
  `country` varchar(255) DEFAULT NULL COMMENT '国家',
  `unionid` varchar(255) DEFAULT NULL COMMENT '开放平台id',
  `c_time` datetime DEFAULT NULL,
  `u_time` datetime DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=48 DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;

-- ----------------------------
-- Records of kt_gptcms_wx_user
-- ----------------------------

-- ----------------------------
-- Table structure for `kt_gptcms_wxgzh`
-- ----------------------------
DROP TABLE IF EXISTS `kt_gptcms_wxgzh`;
CREATE TABLE `kt_gptcms_wxgzh` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `wid` int(10) NOT NULL,
  `appid` varchar(255) DEFAULT NULL,
  `appsecret` varchar(255) DEFAULT NULL,
  `token` varchar(255) DEFAULT NULL COMMENT '令牌',
  `message_key` varchar(255) DEFAULT NULL COMMENT '消息加解密密钥',
  `message_mode` varchar(255) DEFAULT NULL COMMENT '明文加密',
  `type` tinyint(1) DEFAULT '1' COMMENT '1手动配置 2扫码授权',
  `create_time` datetime DEFAULT NULL,
  `update_time` datetime DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;

-- ----------------------------
-- Records of kt_gptcms_wxgzh
-- ----------------------------

-- ----------------------------
-- Table structure for `kt_gptcms_xcx`
-- ----------------------------
DROP TABLE IF EXISTS `kt_gptcms_xcx`;
CREATE TABLE `kt_gptcms_xcx` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `wid` int(11) DEFAULT NULL,
  `title` varchar(255) DEFAULT NULL COMMENT '标题',
  `share_image` varchar(255) DEFAULT NULL COMMENT '分享图片链接',
  `ios_status` tinyint(1) DEFAULT '0' COMMENT 'ios支付 1开启 0关闭',
  `xcx_audit` tinyint(1) DEFAULT NULL COMMENT '小程序审核模式 1开启 0关闭',
  `create_time` datetime DEFAULT NULL,
  `update_time` datetime DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;

-- ----------------------------
-- Records of kt_gptcms_xcx
-- ----------------------------

-- ----------------------------
-- Table structure for `kt_gptcms_xcx_user`
-- ----------------------------
DROP TABLE IF EXISTS `kt_gptcms_xcx_user`;
CREATE TABLE `kt_gptcms_xcx_user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `wid` int(11) DEFAULT NULL COMMENT '系统用户id',
  `common_id` int(11) DEFAULT NULL COMMENT '关联基础用户id',
  `openid` varchar(255) DEFAULT NULL COMMENT 'openid',
  `mobile` varchar(255) DEFAULT NULL COMMENT '手机号',
  `nickname` varchar(255) DEFAULT NULL COMMENT '昵称',
  `headimgurl` varchar(255) DEFAULT NULL COMMENT '头像',
  `sex` tinyint(1) DEFAULT '1' COMMENT '性别 0女 1男',
  `city` varchar(255) DEFAULT NULL COMMENT '市',
  `province` varchar(255) DEFAULT NULL COMMENT '省',
  `country` varchar(255) DEFAULT NULL COMMENT '国家',
  `unionid` varchar(255) DEFAULT NULL COMMENT '开放平台id',
  `c_time` datetime DEFAULT NULL,
  `u_time` datetime DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;

-- ----------------------------
-- Records of kt_gptcms_xcx_user
-- ----------------------------
