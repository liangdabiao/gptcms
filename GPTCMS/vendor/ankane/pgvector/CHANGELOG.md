## 0.1.2 (2023-04-11)

- Fixed cast for Laraval 9

## 0.1.1 (2023-03-11)

- Added `Vector` class
- Added cast for Laravel

## 0.1.0 (2022-08-05)

- First release
