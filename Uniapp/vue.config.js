



module.exports = {
	
	configureWebpack: {
		devServer: {
		
			disableHostCheck: true
		}
	},
	
	chainWebpack: config => {  
		// 忽略编译 config.js  
		config.module.rule('js').exclude.add(/config\.js$/)  
	  }  
	
	
}
