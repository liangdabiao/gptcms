

import Vue from 'vue';
import App from './App';


Vue.config.productionTip = false;

App.mpType = 'app';

// #ifdef H5
if (localStorage.getItem('stopForUrlReplace')) {
	throw new Error('Please wait for redirect...')
}
let widArr = window.location.href.split('?wid=')
let wid = ''
if(widArr.length>1) wid = parseInt(widArr[1])
if(wid) uni.setStorageSync('wid',wid)
// #endif

// 引入全局uView
import uView from 'uview-ui';
Vue.use(uView);

//挂载Jssdk
// import weixin from '@/utils/weixin.js'
// Vue.prototype.$wxsdk = weixin

const app = new Vue({
	...App
});
// window.$vueApp = app;

Vue.prototype.$onLaunched = new Promise(resolve => {
    Vue.prototype.$isResolve = resolve
})

// http拦截器，将此部分放在new Vue()和app.$mount()之间，才能App.vue中正常使用
import httpInterceptor from '@/static/api/http.interceptor.js';
Vue.use(httpInterceptor, app);

// http接口API抽离，免于写url或者一些固定的参数
import httpApi from '@/static/api/http.api.js';
Vue.use(httpApi, app);





app.$mount();







