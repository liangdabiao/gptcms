let systemConfigName = 'system_config'

export const setSystemConfig = (data)=>{
    uni.setStorageSync(systemConfigName,JSON.stringify(data))
}
export const getSystemConfig = ()=>{
    let jsonStr = uni.getStorageSync(systemConfigName)
    if(jsonStr) return JSON.parse(jsonStr)
    return undefined
}

export const getAliPayConfig = ()=>{
    let data = getSystemConfig()
    if(data){
        return data['alipay_status']
    }else{
        return '0'
    }
}

export const getHighDrawConfig = ()=>{
    let data = getSystemConfig()
    if(data){
        return data['paint_draw_status']
    }else{
        return '0'
    }
}