
/**
 * @description 支付
 * @param {array} json 
 * @returns 
 */
import wx from "weixin-js-sdk";
 async function pay (s,json) {
	let obj = json.data
	let isMiniprogram = uni.getStorageSync('isMiniprogram')=='1'

	if(isMiniprogram){
		obj.notpay = 1 
	}

	var r = await s.$u.api[json.api](obj)
	if (r) {
		s.orderId = r.data.order_id
		var orderInfo = r.data
		orderInfo.setmeal_type = obj.setmeal_type
		if(isMiniprogram){
			wx.miniProgram.postMessage({ data: orderInfo });
			setTimeout(()=>{
				// 小程序后退，组件销毁，分享才能收到消息
				wx.miniProgram.navigateBack({delta: 1})
			},400)
			return
		}
		

		if(JSON.stringify(orderInfo).includes('支付成功'))return 0
		// #ifdef APP-PLUS
		var provider = 1
		return await appPay(provider, orderInfo)
		// #endif
		// #ifdef MP-WEIXIN	
		return await weixinPay(orderInfo)
		// #endif

		// #ifdef H5
		let isMobile = /(iPhone|iPad|iPod|iOS|Android|Linux armv8l|Linux armv7l|Linux aarch64)/i.test(navigator.platform);
			
		var ua = window.navigator.userAgent.toLowerCase();
		let val = ua.match(/MicroMessenger/i) == 'micromessenger'
		console.log('gggggssssssscsdfsdf',orderInfo)
		if(val){
			// 微信浏览器
			if(isMobile){
				return await h5Pay(orderInfo)
			}else{
				return { 
					isTimer:true,
					showPic:true,
					code_url:orderInfo.payconfig.code_url

				}
			}
			
		}else{
			if(isMobile){
				// 手机
				console.log('gggggsssssss',orderInfo)
				window.location.href = orderInfo.payconfig.mweb_url
				return { 
					isTimer:true
				}
			}else{
				// pc显示二维码
				
				return { 
					isTimer:true,
					showPic:true,
					code_url:orderInfo.payconfig.code_url

				}
			}

			
		}
		// #endif
	} else {
		return false
	}
}

function weixinPay (orderInfo) {
	return new Promise((resolve) => {
		uni.requestPayment({
			// ...orderInfo,

			timeStamp: orderInfo.payconfig.timeStamp,
			nonceStr: orderInfo.payconfig.nonceStr,
			package: orderInfo.payconfig.package,
			paySign: orderInfo.payconfig.paySign,
			signType: orderInfo.payconfig.signType,
			success: () => {
				console.log(98)
				resolve(true)
			},
			fail: () => {
				console.log(102)
				resolve(false)
			},
		})
	})
}

function appPay (provider, orderInfo) {
	return new Promise((resolve) => {
		uni.requestPayment({
			provider,
			orderInfo: orderInfo,
			success: (e) => {
				resolve(true)
			},
			fail: (e) => {
				resolve(false)
			},
		})
	})
}

function h5Pay (orderInfo) {
	return new Promise((resolve) => {
		if (typeof WeixinJSBridge == "undefined") {
			if (document.addEventListener) {
				document.addEventListener('WeixinJSBridgeReady', onBridgeReady, false);
			} else if (document.attachEvent) {
				document.attachEvent('WeixinJSBridgeReady', onBridgeReady);
				document.attachEvent('onWeixinJSBridgeReady', onBridgeReady);
			}
		} else {
			// resolve(true)
			; (async () => {
				var isSuccess = await onBridgeReady(orderInfo)
				if (isSuccess) {
					resolve(true)
				} else {
					resolve(false)
				}
			})()
		}
	})
}
//h5唤起微信支付
function onBridgeReady (orderInfo) {
	return new Promise((resolve) => {
		WeixinJSBridge.invoke(
			'getBrandWCPayRequest', {
			"appId": orderInfo.payconfig.appId, //公众号名称，由商户传入     
			"timeStamp": orderInfo.payconfig.timeStamp, //时间戳，自1970年以来的秒数     
			"nonceStr": orderInfo.payconfig.nonceStr, //随机串     
			"package": orderInfo.payconfig.package,
			"signType": orderInfo.payconfig.signType, //微信签名方式： 
			"paySign": orderInfo.payconfig.paySign //微信签名 
		},
			function (res) { 
				if (res.err_msg == "get_brand_wcpay_request:ok") {
					resolve(true)
				} else if (res.err_msg === 'get_brand_wcpay_request:cancel') {
					resolve(false)
				}
			}
		);
	})
}

//是否为0
function isZero(num){ 
	return !uni.$u.test.isEmpty(num) || !isNaN(num)
}

export default {
	pay,
	weixinPay,
	isZero
};