# :star: GPTCMS长期更新，您的 :star: star是我们前进的动力

 [安装教程](https://kqv9wk0r7t.feishu.cn/wiki/HhmMwCrYuiXoSCkuuBHcuCPJnqf) | [接口文档](https://kqv9wk0r7t.feishu.cn/wiki/LXGmwTYWZiCvrIk6V65c1pkCn1f) | [应用开发](https://www.showdoc.com.cn/ktadmin/9324421497854622) | [狂团官网](https://www.kt8.cn) | [KtAdmin官网](http://www.ktadmin.cn/) | [AI大模型训练](https://www.kt8.cn/item/view4748.html)

### 系统推荐（AI大模型训练系统：）

https://www.kt8.cn/item/view4748.html

###  **GPTCMS用户（开发者）交流群 **

![输入图片说明](https://kt8logo.oss-cn-beijing.aliyuncs.com/gptcms-free.png)




# GPTCMS = GPT Chat Management System（GPT聊天管理系统）

#### 介绍
GPTCMS基于TP6+Uniapp+VUE3开发，已集成文心一言、灵犀星火、ChatGLM（清华旗下）、讯飞星火（即将）、api2d、意间AI、ChatGPT、GPT3.5、GPT4、Stable Diffusion、MidJourney-V4、MidJourney-V5.1等人工智能技术，免费版支持PC、H5、公众号等多端，自带安全审核机制。部署后即为SAAS系统，可无限搭建GPT平台。狂团软件商城 www.kt8.cn


# 其他安装方式

1.在线一键安装 https://www.kt8.cn/item/view4248.html

2.宝塔一键部署，搜索gptcms，即可一键安装



### 功能特性

1.系统底层为SAAS，部署后可无限开账号，无限搭建

2.支持连续对话

3.已对接GPT3.5、GPT4、灵犀星火、API2D、文心一言、ChatGLM等AI对话平台，及 SD、MJ、意间、灵犀星火等AI绘画平台，后续还将持续对接昆仑天工、讯飞星火、通义千问等平台，各平台接口支持一键切换

4.免费版支持 微信H5端（JSAPI支付），原生H5端（H5支付），PC端（二维码支付），付费版额外支持微信小程序端

5.同时支持国内AI聊天接口，不用国内国外服务器也能用，使用更省心！

6.多重内容安全机制设计，运营更安心！

1）支持同时开启 系统敏感词库+百度文本审核，双重审核机制

2）系统自带敏感词库，同时支持平台方在后台添加敏感词

3）支持敏感词自动处理为*或阻止提交，双重处理机制

4）支持拉黑用户，拉黑后无法登录和聊天

7.后台支持自由添加创作模型、私人助理模型等，同时支持prompt、语言自定义变量

8.同时支持百度AI文本审核、腾讯云语音转文字、阿里云文字转语音、科大讯飞文字转语音等接口

9.支持VIP会员充值、条数充值、注册赠送、每日赠送等多种营销功能，赚钱更贴心！

10.创作和私人助理模型支持默认数据导入，使用更贴心！

11.业务端支持一键复制，一键清除历史消息等功能

12.系统已内置前置指令，可自行修改，让AI更智能，更贴心，更合规

13.支持将某些创作或私人助理模型设置为VIP模型，仅限VIP可用

14.创作或私人助理模型 支持个性化添加欢迎语

15.支持 AI输出内容自动转为语音播放

16.支持后台手动为用户开通VIP，手动为用户充值条数等

17.注册支持短信验证码登录（对接阿里云短信）

18.支持一次性回复、流式回复等

19.更多强大功能火速迭代中


### 开源说明

后端无加密、Uniapp端完全开源、PC后台VUE端免费版提供编译后代码，商业版可提供编译前代码，免费版禁止去除GPTCMS相关版权


### 商业版购买

开源不易，以下如果有需要，请支持一下，感谢您的支持，让我们有更多动力！

商业版购买地址 [https://www.kt8.cn/item/view2067.html](http://www.kt8.cn/item/view2067.html)



### 开发者生态

GPTCMS自带开发者生态，欢迎开发者基于本系统开发更多优质模板、应用，对于优质作品狂团平台可协助推广出售、帮助开发者变现

欢迎登录狂团平台PC端首页，安装喜欢的模板和应用[https://www.kt8.cn](http://www.kt8.cn)


### 使用须知

1.允许用于个人学习、毕业设计、教学案例、公益事业;

2.免费版使用必须保留版权信息，请自觉遵守;

3.Uniapp端完全开源，允许GPTCMS开发者任意二开或二开后作为新模板、应用等再次上架狂团平台出售

4.如需商业用途，请购买商业版（或完成相关推广任务）。



### 版权信息


本项目包含的第三方源码和二进制文件之版权信息另行标注。

版权所有Copyright © 2023 by GPTCMS (http://www.gptcms.cn) 

All rights reserved。

### 系统演示


![输入图片说明](https://addon8.oss-cn-shenzhen.aliyuncs.com/config/ueditor/php/upload/image/20230515/1684124775467576.png)![输入图片说明](https://addon8.oss-cn-shenzhen.aliyuncs.com/config/ueditor/php/upload/image/20230515/1684124776943815.png)![输入图片说明](https://addon8.oss-cn-shenzhen.aliyuncs.com/config/ueditor/php/upload/image/20230515/1684124776440260.png)![输入图片说明](https://addon8.oss-cn-shenzhen.aliyuncs.com/config/ueditor/php/upload/image/20230515/1684124777997806.png)